package com.epam.esm.util;

import com.epam.esm.pageparameter.PageParameter;

import java.util.HashMap;
import java.util.Map;

public class PaginationUtil {
    private PaginationUtil() {
    }

    public static PageParameter getLastPage(PageParameter current) {
        return new PageParameter(current.getTotalPageCount(), current.getLimit(), current.getOrderBy());
    }

    public static PageParameter getPreviousPage(PageParameter current) {
        return new PageParameter(current.getPreviousPage(), current.getLimit(), current.getOrderBy());
    }

    public static PageParameter getFirstPage(PageParameter current) {
        return new PageParameter(1, current.getLimit(), current.getOrderBy());
    }

    public static PageParameter getNextPage(PageParameter current) {
        return new PageParameter(current.getNextPage(), current.getLimit(), current.getOrderBy());
    }

    public static Map<String, String> getFirstPageParameters(Map<String, String> parameters, PageParameter pageParameter) {
        HashMap<String, String> result = new HashMap<>(parameters);
        result.put("page", "1");
        result.put("limit", String.valueOf(pageParameter.getLimit()));
        return result;
    }

    public static Map<String, String> getPreviousPageParameters(Map<String, String> parameters, PageParameter pageParameter) {
        HashMap<String, String> result = new HashMap<>(parameters);
        result.put("page", String.valueOf(pageParameter.getPreviousPage()));
        result.put("limit", String.valueOf(pageParameter.getLimit()));
        return result;
    }

    public static Map<String, String> getNextPageParameters(Map<String, String> parameters, PageParameter pageParameter) {
        HashMap<String, String> result = new HashMap<>(parameters);
        result.put("page", String.valueOf(pageParameter.getNextPage()));
        result.put("limit", String.valueOf(pageParameter.getLimit()));
        return result;
    }

    public static Map<String, String> getLastPageParameters(Map<String, String> parameters, PageParameter pageParameter) {
        HashMap<String, String> result = new HashMap<>(parameters);
        result.put("page", String.valueOf(pageParameter.getTotalPageCount()));
        result.put("limit", String.valueOf(pageParameter.getLimit()));
        return result;
    }
}