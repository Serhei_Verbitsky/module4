package com.epam.esm.config;

import com.epam.esm.dto.UserRole;
import com.epam.esm.entity.Role;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfiguration {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setPropertyCondition(Conditions.isNotNull())
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);
        modelMapper.typeMap(UserRole.class, Role.class)
                .setConverter(context -> new Role(context.getSource().getName()));
        modelMapper.typeMap(Role.class, UserRole.class)
                .setConverter(context -> UserRole.resolve(context.getSource().getName()));
        return modelMapper;
    }
}