package com.epam.esm.config;

import com.epam.esm.security.BasicAccessDeniedHandler;
import com.epam.esm.security.FailureAuthEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Autowired
    private FailureAuthEntryPoint failureAuthEntryPoint;

    @Autowired
    private BasicAccessDeniedHandler basicAccessDeniedHandler;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
         .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                    .antMatchers(GET,"/certificates/**").permitAll()
                    .antMatchers(POST,"/users/sign-up").permitAll()
                    .anyRequest().authenticated()
                .and()
                    .cors()
                .and()
                    .exceptionHandling().authenticationEntryPoint(failureAuthEntryPoint)
                    .accessDeniedHandler(basicAccessDeniedHandler)
                .and()
                    .csrf().disable();
    }
}