package com.epam.esm.errorresponse;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorInfoResponse {
    private List<String> message;
    private String statusCode;
    private String query;

    public void addMessage(String message) {
        getMessage().add(message);
    }

    public ErrorInfoResponse() {
        this.message = new ArrayList<>();
    }
}