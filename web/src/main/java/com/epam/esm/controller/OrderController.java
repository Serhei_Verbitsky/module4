package com.epam.esm.controller;

import com.epam.esm.converter.impl.OrderSaveRequest;
import com.epam.esm.dto.CertificateDto;
import com.epam.esm.dto.OrderDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.dto.UserDto;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.epam.esm.util.PaginationUtil.getFirstPage;
import static com.epam.esm.util.PaginationUtil.getFirstPageParameters;
import static com.epam.esm.util.PaginationUtil.getLastPage;
import static com.epam.esm.util.PaginationUtil.getLastPageParameters;
import static com.epam.esm.util.PaginationUtil.getNextPage;
import static com.epam.esm.util.PaginationUtil.getNextPageParameters;
import static com.epam.esm.util.PaginationUtil.getPreviousPage;
import static com.epam.esm.util.PaginationUtil.getPreviousPageParameters;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@Validated
@RequestMapping("/orders")
public class OrderController {
    private final OrderService service;
    private final Environment env;

    @Autowired
    public OrderController(OrderService service, Environment environment) {
        this.service = service;
        env = environment;
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<OrderDto>> findAll(
            @RequestParam(required = false) Map<String, String> parameterMap,
            @Valid PageParameter pageParameter) {

        List<OrderDto> orders = service.findAll(parameterMap, pageParameter);
        orders.forEach(order -> {
            order.add(linkTo(methodOn(OrderController.class).findById(null, order.getId()))
                    .withRel(env.getProperty("link.order.current"))
                    .withType(HttpMethod.GET.name()));
            addCertificateLinks(order.getCertificates());
            addUserLinks(order.getUser());
        });
        orders.add(createOrderWithPaginationInfo(parameterMap, pageParameter));
        return ResponseEntity.ok(orders);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<EntityModel<OrderDto>> findById(Authentication authentication,
                                                          @PathVariable("id") @Min(value = 1, message = "40003") Long id) {
        OrderDto result = service.findById(id, authentication);
        addCertificateLinks(result.getCertificates());
        addUserLinks(result.getUser());
        return ResponseEntity.ok(EntityModel.of(result,
                linkTo(methodOn(OrderController.class).create(new OrderSaveRequest(), null))
                        .withRel(env.getProperty("link.order.create"))
                        .withType(HttpMethod.POST.name())));
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping
    public ResponseEntity<EntityModel<OrderDto>> create(@Valid @RequestBody OrderSaveRequest orderSaveRequest,
                                                        Authentication authentication) {
        OrderDto result = service.save(orderSaveRequest, authentication);
        addCertificateLinks(result.getCertificates());
        addUserLinks(result.getUser());
        return ResponseEntity.status(HttpStatus.CREATED).body(EntityModel.of(result,
                linkTo(methodOn(OrderController.class).findById(null, result.getId()))
                        .withRel(env.getProperty("link.order.current"))
                        .withType(HttpMethod.GET.name())));
    }

    private void addUserLinks(UserDto user) {
        if (user != null) {
            user.add(linkTo(methodOn(UserController.class).findById(null, user.getId()))
                    .withRel(env.getProperty("link.user.current"))
                    .withType(HttpMethod.GET.name()));
        }
    }

    private void addCertificateLinks(List<CertificateDto> certificates) {
        certificates.forEach(certificate -> {
            certificate.add(linkTo(methodOn(CertificateController.class).findById(certificate.getId()))
                    .withRel(env.getProperty("link.certificate.current"))
                    .withType(HttpMethod.GET.name()));
            addTagLinks(certificate.getTags());
        });
    }

    private void addTagLinks(Set<TagDto> tags) {
        tags.forEach((tag -> tag.add(linkTo(methodOn(TagController.class).findById(tag.getId()))
                .withRel(env.getProperty("link.tag.current"))
                .withType(HttpMethod.GET.name()))));
    }

    private OrderDto createOrderWithPaginationInfo(Map<String, String> parameterMap, PageParameter pageParameter) {
        OrderDto lastOrder = new OrderDto();
        if (pageParameter.getPage() >= 2) {
            lastOrder.add(linkTo(methodOn(OrderController.class).findAll(getFirstPageParameters(parameterMap, pageParameter), getFirstPage(pageParameter)))
                    .withRel(env.getProperty("page.first"))
                    .withType(HttpMethod.GET.name()));
        }
        if (pageParameter.getPreviousPage() >= 2 && pageParameter.getNextPage() < pageParameter.getTotalPageCount()) {
            lastOrder.add(linkTo(methodOn(OrderController.class).findAll(getPreviousPageParameters(parameterMap, pageParameter), getPreviousPage(pageParameter)))
                    .withRel(env.getProperty("page.previous"))
                    .withType(HttpMethod.GET.name()));
        }
        lastOrder.add(linkTo(methodOn(OrderController.class).findAll(parameterMap, pageParameter))
                .withRel(env.getProperty("page.current"))
                .withType(HttpMethod.GET.name()));
        if (pageParameter.getNextPage() < pageParameter.getTotalPageCount()) {
            lastOrder.add(linkTo(methodOn(OrderController.class).findAll(getNextPageParameters(parameterMap, pageParameter), getNextPage(pageParameter)))
                    .withRel(env.getProperty("page.next"))
                    .withType(HttpMethod.GET.name()));
        }
        if (pageParameter.getTotalPageCount() > (pageParameter.getPage())) {
            lastOrder.add(linkTo(methodOn(OrderController.class).findAll(getLastPageParameters(parameterMap, pageParameter), getLastPage(pageParameter)))
                    .withRel(env.getProperty("page.last"))
                    .withType(HttpMethod.GET.name()));
        }
        return lastOrder;
    }
}