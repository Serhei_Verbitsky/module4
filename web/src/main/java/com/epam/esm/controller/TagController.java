package com.epam.esm.controller;

import com.epam.esm.dto.TagDto;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.service.TagService;
import com.epam.esm.validator.OnCreate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.epam.esm.util.PaginationUtil.getFirstPage;
import static com.epam.esm.util.PaginationUtil.getFirstPageParameters;
import static com.epam.esm.util.PaginationUtil.getLastPage;
import static com.epam.esm.util.PaginationUtil.getLastPageParameters;
import static com.epam.esm.util.PaginationUtil.getNextPage;
import static com.epam.esm.util.PaginationUtil.getNextPageParameters;
import static com.epam.esm.util.PaginationUtil.getPreviousPage;
import static com.epam.esm.util.PaginationUtil.getPreviousPageParameters;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@Validated
@RequestMapping("/tags")
public class TagController {
    private final TagService service;
    private final Environment env;

    @Autowired
    public TagController(TagService service, Environment environment) {
        this.service = service;
        env = environment;
    }

    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public ResponseEntity<List<TagDto>> findAll(
            @RequestParam(required = false) Map<String, String> parameterMap,
            @Valid PageParameter pageParameter) {
        List<TagDto> tags = service.findAll(parameterMap, pageParameter);
        tags.forEach(tag -> tag.add(
                linkTo(methodOn(TagController.class).findById(tag.getId()))
                        .withSelfRel()
                        .withType(HttpMethod.GET.name())));
        tags.add(createTagWithPaginationInfo(parameterMap, pageParameter));
        return ResponseEntity.ok(tags);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public ResponseEntity<EntityModel<TagDto>> findById
            (@Valid @PathVariable("id") @Min(value = 1, message = "40003") Long id) {
        TagDto result = service.findById(id);
        return ResponseEntity.ok(EntityModel.of(result,
                linkTo(methodOn(TagController.class).findById(id))
                        .withSelfRel()
                        .withType(HttpMethod.GET.name()),
                linkTo(methodOn(TagController.class).create(result))
                        .withRel(env.getProperty("link.tag.create"))
                        .withType(HttpMethod.POST.name()),
                linkTo(methodOn(TagController.class).delete(id))
                        .withRel(env.getProperty("link.tag.delete"))
                        .withType(HttpMethod.DELETE.name())));
    }

    @PostMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Validated(OnCreate.class)
    public ResponseEntity<EntityModel<TagDto>> create(@Valid @RequestBody TagDto dto) {
        TagDto result = service.findOrSave(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(EntityModel.of(result,
                linkTo(methodOn(TagController.class).findById(result.getId()))
                        .withRel(env.getProperty("link.tag.current"))
                        .withType(HttpMethod.GET.name()),
                linkTo(methodOn(TagController.class).create(result))
                        .withSelfRel()
                        .withType(HttpMethod.POST.name()),
                linkTo(methodOn(TagController.class).delete(result.getId()))
                        .withRel(env.getProperty("link.tag.delete"))
                        .withType(HttpMethod.DELETE.name())));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<EntityModel<TagDto>> delete(@Valid @PathVariable("id") @Min(value = 1, message = "40003") Long id) {
        service.delete(id);
        return ResponseEntity.ok(EntityModel.of(new TagDto(),
                linkTo(methodOn(TagController.class)
                        .findAll(new HashMap<>(), new PageParameter(1, 10, new ArrayList<>())))
                        .withRel(env.getProperty("link.tag.all"))));
    }

    @GetMapping("/most-used")
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public ResponseEntity<EntityModel<TagDto>> findMostUsedTag() {
        TagDto result = service.findUserMostUsedTag();
        return ResponseEntity.ok(EntityModel.of(result));
    }

    private TagDto createTagWithPaginationInfo(Map<String, String> parameterMap, PageParameter pageParameter) {
        TagDto lastTag = new TagDto();
        if (pageParameter.getPage() >= 2) {
            lastTag.add(linkTo(methodOn(TagController.class).findAll(getFirstPageParameters(parameterMap, pageParameter), getFirstPage(pageParameter)))
                    .withRel(env.getProperty("page.first"))
                    .withType(HttpMethod.GET.name()));
        }
        if (pageParameter.getPreviousPage() >= 2 && pageParameter.getNextPage() < pageParameter.getTotalPageCount()) {
            lastTag.add(linkTo(methodOn(TagController.class).findAll(getPreviousPageParameters(parameterMap, pageParameter), getPreviousPage(pageParameter)))
                    .withRel(env.getProperty("page.previous"))
                    .withType(HttpMethod.GET.name()));
        }
        lastTag.add(linkTo(methodOn(TagController.class).findAll(parameterMap, pageParameter))
                .withRel(env.getProperty("page.current"))
                .withType(HttpMethod.GET.name()));
        if (pageParameter.getNextPage() < pageParameter.getTotalPageCount()) {
            lastTag.add(linkTo(methodOn(TagController.class).findAll(getNextPageParameters(parameterMap, pageParameter), getNextPage(pageParameter)))
                    .withRel(env.getProperty("page.next"))
                    .withType(HttpMethod.GET.name()));
        }
        if (pageParameter.getTotalPageCount() > (pageParameter.getPage())) {
            lastTag.add(linkTo(methodOn(TagController.class).findAll(getLastPageParameters(parameterMap, pageParameter), getLastPage(pageParameter)))
                    .withRel(env.getProperty("page.last"))
                    .withType(HttpMethod.GET.name()));
        }
        return lastTag;
    }
}