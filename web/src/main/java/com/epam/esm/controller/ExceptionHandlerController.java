package com.epam.esm.controller;

import com.epam.esm.errorresponse.ErrorInfoResponse;
import com.epam.esm.exception.AbstractException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.epam.esm.exception.OperationError.BAD_REQUEST_MULTIPLE_PARAMETERS_ERROR;
import static com.epam.esm.exception.OperationError.BAD_REQUEST_PARAMETER;

@RestControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {
    private static final String DEFAULT_BAD_REQUEST_CODE = "40000";
    private final MessageSource messageSource;

    @Autowired
    public ExceptionHandlerController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(AbstractException.class)
    public ResponseEntity<ErrorInfoResponse> handleServiceError(AbstractException exception,
                                                                Locale locale, HttpServletRequest request) {
        List<Pair<String, String>> errors = new ArrayList<>();
        errors.add(Pair.of(exception.getOperationError().getErrorCode(),
                String.join("", exception.getErrorDetails())));
        return buildErrorInfoResponse(errors, request, locale);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return processValidationError(ex, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return processValidationError(ex, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<Pair<String, String>> errors = new ArrayList<>();
        String errorValue = ((InvalidFormatException)ex.getCause()).getValue().toString();
        errors.add(Pair.of(String.valueOf(BAD_REQUEST_PARAMETER.getErrorCode()), errorValue));
        HttpServletRequest req = ((ServletWebRequest) request).getRequest();
        return new ResponseEntity<>(
                buildErrorInfoResponseBody(errors, req, request.getLocale()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorInfoResponse> handleServiceError(ConstraintViolationException exception,
                                                                Locale locale, HttpServletRequest request) {
        List<Pair<String, String>> errors = new ArrayList<>();
        exception.getConstraintViolations().forEach(constraint -> {
            String wrongValue = String.valueOf(constraint.getInvalidValue());
            errors.add(Pair.of(constraint.getMessage(), wrongValue));
        });
        return buildErrorInfoResponse(errors, request, locale);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
                                                        HttpStatus status, WebRequest request) {
        HttpServletRequest req = ((ServletWebRequest) request).getRequest();
        List<Pair<String, String>> errors = new ArrayList<>();
        errors.add(Pair.of(BAD_REQUEST_PARAMETER.getErrorCode(), (String.valueOf(ex.getValue()))));
        return new ResponseEntity<>(
                buildErrorInfoResponseBody(errors, req, request.getLocale()), HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<Object> processValidationError(BindException ex, WebRequest request) {
        List<Pair<String, String>> errors = new ArrayList<>();
        List<ObjectError> exceptionErrors = ex.getAllErrors();
        List<FieldError> fields = ex.getFieldErrors();
        int count = (Math.min(exceptionErrors.size(), fields.size()));
        for (int i = 0; i < count; i++) {
            errors.add(Pair.of(exceptionErrors.get(i).getDefaultMessage(), String.valueOf(fields.get(i).getRejectedValue())));
        }
        HttpServletRequest req = ((ServletWebRequest) request).getRequest();
        return new ResponseEntity<>(
                buildErrorInfoResponseBody(errors, req, request.getLocale()), HttpStatus.BAD_REQUEST);
    }

    private ErrorInfoResponse buildErrorInfoResponseBody(List<Pair<String, String>> errors, HttpServletRequest request,
                                                         Locale locale) {

        ErrorInfoResponse errorInfoResponse = new ErrorInfoResponse();
        try {
            errors.forEach((pair) -> {
                String msg = messageSource.getMessage(pair.getFirst(), new Object[]{}, locale);
                String localizedMessage = new MessageFormat(msg, locale).format(new String[]{pair.getSecond()});
                errorInfoResponse.addMessage(localizedMessage);
                errorInfoResponse.setStatusCode(pair.getFirst());
            });
        } catch (Exception exception) {
            errorInfoResponse.getMessage().add(messageSource.getMessage(DEFAULT_BAD_REQUEST_CODE, new Object[]{}, locale));
            errorInfoResponse.setStatusCode(DEFAULT_BAD_REQUEST_CODE);
        }
        if (errors.size() > 1) {
            errorInfoResponse.setStatusCode(BAD_REQUEST_MULTIPLE_PARAMETERS_ERROR.getErrorCode());
        }
        errorInfoResponse.setQuery(request.getRequestURL().toString() + "?" +
                (request.getQueryString() == null ? "" : request.getQueryString()));
        return errorInfoResponse;
    }

    private ResponseEntity<ErrorInfoResponse> buildErrorInfoResponse(List<Pair<String, String>> errors, HttpServletRequest request,
                                                                     Locale locale) {

        ErrorInfoResponse errorBody = buildErrorInfoResponseBody(errors, request, locale);
        return new ResponseEntity<>(errorBody,
                HttpStatus.resolve(Integer.parseInt(errorBody.getStatusCode().substring(0,3))));
    }
}