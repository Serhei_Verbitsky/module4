package com.epam.esm.controller;

import com.epam.esm.dto.CertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.validator.OnCreate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.epam.esm.util.PaginationUtil.getFirstPage;
import static com.epam.esm.util.PaginationUtil.getFirstPageParameters;
import static com.epam.esm.util.PaginationUtil.getLastPage;
import static com.epam.esm.util.PaginationUtil.getLastPageParameters;
import static com.epam.esm.util.PaginationUtil.getNextPage;
import static com.epam.esm.util.PaginationUtil.getNextPageParameters;
import static com.epam.esm.util.PaginationUtil.getPreviousPage;
import static com.epam.esm.util.PaginationUtil.getPreviousPageParameters;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/certificates")
@Validated
public class CertificateController {
    private final GiftCertificateService service;
    private final Environment env;

    @Autowired
    public CertificateController(GiftCertificateService service, Environment environment) {
        this.service = service;
        env = environment;
    }

    @GetMapping
    public ResponseEntity<List<CertificateDto>> findAll(
            @RequestParam(required = false) Map<String, String> parameterMap,
            @Valid PageParameter pageParameter) {

        List<CertificateDto> certificates = service.findAll(parameterMap, pageParameter);
        certificates.forEach(certificate -> {
            addTagLinks(certificate.getTags());
            certificate.add(linkTo(methodOn(CertificateController.class).findById(certificate.getId()))
                    .withRel(env.getProperty("link.certificate.current"))
                    .withType(HttpMethod.GET.name()));
        });
        certificates.add(createCertificateWithPaginationInfo(parameterMap, pageParameter));
        return ResponseEntity.ok(certificates);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CertificateDto> findById(@PathVariable("id") @Min(value = 1, message = "40003") Long id) {
        CertificateDto result = service.findById(id);
        result.add(linkTo(methodOn(CertificateController.class).findById(id))
                        .withSelfRel()
                        .withType(HttpMethod.GET.name()),
                linkTo(methodOn(CertificateController.class).update(id, null))
                        .withRel("link.certificate.update")
                        .withType(HttpMethod.PUT.name()),
                linkTo(methodOn(CertificateController.class).patch(id, null))
                        .withRel(env.getProperty("link.certificate.patch"))
                        .withType(HttpMethod.PATCH.name()),
                linkTo(methodOn(CertificateController.class).delete(id))
                        .withRel(env.getProperty("link.certificate.delete"))
                        .withType(HttpMethod.DELETE.name()),
                linkTo(methodOn(CertificateController.class)
                        .findAll(new HashMap<>(), new PageParameter(1, 10, new ArrayList<>())))
                        .withRel(env.getProperty("link.certificate.all"))
                        .withType(HttpMethod.GET.name()));
        addTagLinks(result.getTags());
        return ResponseEntity.ok(result);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<EntityModel<CertificateDto>> update(@PathVariable("id") Long id, @RequestBody CertificateDto certificateDto) {
        certificateDto.setId(id);
        service.update(certificateDto);
        return ResponseEntity.ok(EntityModel.of(new CertificateDto(),
                linkTo(methodOn(CertificateController.class).findById(id))
                        .withRel(env.getProperty("link.certificate.current"))
                        .withType(HttpMethod.GET.name()),
                linkTo(methodOn(CertificateController.class).update(id, null))
                        .withSelfRel()
                        .withType(HttpMethod.PUT.name()),
                linkTo(methodOn(CertificateController.class).patch(id, null))
                        .withRel(env.getProperty("link.certificate.patch"))
                        .withType(HttpMethod.PATCH.name()),
                linkTo(methodOn(CertificateController.class).delete(id))
                        .withRel(env.getProperty("link.certificate.delete"))
                        .withType(HttpMethod.DELETE.name()),
                linkTo(methodOn(CertificateController.class)
                        .findAll(new HashMap<>(), new PageParameter(1, 10, new ArrayList<>())))
                        .withRel(env.getProperty("link.certificate.all"))
                        .withType(HttpMethod.GET.name())));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<EntityModel<CertificateDto>> delete(@PathVariable("id") Long id) {
        service.delete(id);
        return ResponseEntity.ok(EntityModel.of(new CertificateDto(),
                linkTo(methodOn(CertificateController.class)
                        .findAll(new HashMap<>(), new PageParameter(1, 10, new ArrayList<>())))
                        .withRel(env.getProperty("link.certificate.all"))));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping()
    @Validated(OnCreate.class)
    public ResponseEntity<CertificateDto> create(@Valid @RequestBody CertificateDto dto) {
        CertificateDto result = service.save(dto);
        result.add(linkTo(methodOn(CertificateController.class).findById(result.getId()))
                        .withRel(env.getProperty("link.certificate.current"))
                        .withType(HttpMethod.GET.name()),
                linkTo(methodOn(CertificateController.class).create(result))
                        .withSelfRel()
                        .withType(HttpMethod.POST.name()),
                linkTo(methodOn(CertificateController.class).delete(result.getId()))
                        .withRel(env.getProperty("link.certificate.delete"))
                        .withType(HttpMethod.DELETE.name()));
        addTagLinks(result.getTags());
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<EntityModel<CertificateDto>> patch(@PathVariable("id") Long id, @RequestBody CertificateDto certificateDto) {
        certificateDto.setId(id);
        service.patch(certificateDto);
        return ResponseEntity.ok(EntityModel.of(new CertificateDto(),
                linkTo(methodOn(CertificateController.class).findById(id))
                        .withRel(env.getProperty("link.certificate.current"))
                        .withType(HttpMethod.GET.name()),
                linkTo(methodOn(CertificateController.class).update(id, null))
                        .withRel("link.certificate.update")
                        .withType(HttpMethod.PUT.name()),
                linkTo(methodOn(CertificateController.class).patch(id, null))
                        .withSelfRel()
                        .withType(HttpMethod.PATCH.name()),
                linkTo(methodOn(CertificateController.class).delete(id))
                        .withRel(env.getProperty("link.certificate.delete"))
                        .withType(HttpMethod.DELETE.name()),
                linkTo(methodOn(CertificateController.class)
                        .findAll(new HashMap<>(), new PageParameter(1, 10, new ArrayList<>())))
                        .withRel(env.getProperty("link.certificate.all"))
                        .withType(HttpMethod.GET.name())));
    }

    private void addTagLinks(Set<TagDto> tags) {
        tags.forEach((tag -> tag.add(linkTo(methodOn(TagController.class).findById(tag.getId()))
                .withRel(env.getProperty("link.tag.current"))
                .withType(HttpMethod.GET.name()))));
    }

    private CertificateDto createCertificateWithPaginationInfo(Map<String, String> parameterMap, PageParameter pageParameter) {
        CertificateDto lastCertificate = new CertificateDto();
        if (pageParameter.getPage() >= 2) {
            lastCertificate.add(linkTo(methodOn(CertificateController.class).findAll(getFirstPageParameters(parameterMap, pageParameter), getFirstPage(pageParameter)))
                    .withRel(env.getProperty("page.first"))
                    .withType(HttpMethod.GET.name()));
        }
        if (pageParameter.getPreviousPage() >= 2 && pageParameter.getNextPage() < pageParameter.getTotalPageCount()) {
            lastCertificate.add(linkTo(methodOn(CertificateController.class).findAll(getPreviousPageParameters(parameterMap, pageParameter), getPreviousPage(pageParameter)))
                    .withRel(env.getProperty("page.previous"))
                    .withType(HttpMethod.GET.name()));
        }
        lastCertificate.add(linkTo(methodOn(CertificateController.class).findAll(parameterMap, pageParameter))
                .withRel(env.getProperty("page.current"))
                .withType(HttpMethod.GET.name()));
        if (pageParameter.getNextPage() < pageParameter.getTotalPageCount()) {
            lastCertificate.add(linkTo(methodOn(CertificateController.class).findAll(getNextPageParameters(parameterMap, pageParameter), getNextPage(pageParameter)))
                    .withRel(env.getProperty("page.next"))
                    .withType(HttpMethod.GET.name()));
        }
        if (pageParameter.getTotalPageCount() > (pageParameter.getPage())) {
            lastCertificate.add(linkTo(methodOn(CertificateController.class).findAll(getLastPageParameters(parameterMap, pageParameter), getLastPage(pageParameter)))
                    .withRel(env.getProperty("page.last"))
                    .withType(HttpMethod.GET.name()));
        }
        return lastCertificate;
    }
}