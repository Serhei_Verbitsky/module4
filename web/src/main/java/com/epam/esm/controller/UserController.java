package com.epam.esm.controller;

import com.epam.esm.dto.CertificateDto;
import com.epam.esm.dto.OrderDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.dto.UserDto;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.service.UserService;
import com.epam.esm.validator.OnCreate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.epam.esm.util.PaginationUtil.getFirstPage;
import static com.epam.esm.util.PaginationUtil.getFirstPageParameters;
import static com.epam.esm.util.PaginationUtil.getLastPage;
import static com.epam.esm.util.PaginationUtil.getLastPageParameters;
import static com.epam.esm.util.PaginationUtil.getNextPage;
import static com.epam.esm.util.PaginationUtil.getNextPageParameters;
import static com.epam.esm.util.PaginationUtil.getPreviousPage;
import static com.epam.esm.util.PaginationUtil.getPreviousPageParameters;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@Validated
@RequestMapping("/users")
public class UserController {
    private final UserService service;
    private final Environment env;

    @Autowired
    public UserController(UserService service, Environment environment) {
        this.service = service;
        env = environment;
    }

    @GetMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<UserDto>> findAll(
            @RequestParam(required = false) Map<String, String> parameterMap,
            @Valid PageParameter pageParameter) {
        List<UserDto> users = service.findAll(parameterMap, pageParameter);
        users.forEach(user -> {
            user.add(linkTo(methodOn(UserController.class).findById(null, user.getId()))
                    .withRel(env.getProperty("link.user.current"))
                    .withType(HttpMethod.GET.name()));
            addOrderLinks(user.getOrders());
        });
        users.add(createUserWithPaginationInfo(parameterMap, pageParameter));
        return ResponseEntity.ok(users);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<EntityModel<UserDto>> findById(Authentication authentication,
                                                         @PathVariable("id") @Min(value = 1, message = "40003") Long id) {
        UserDto result = service.findById(id, authentication);
        addOrderLinks(result.getOrders());
        return ResponseEntity.ok(EntityModel.of(result,
                linkTo(methodOn(UserController.class).findById(null, result.getId()))
                        .withSelfRel()
                        .withType(HttpMethod.GET.name())));
    }

    @PostMapping("/sign-up")
    @Validated(OnCreate.class)
    public ResponseEntity<EntityModel<UserDto>> create(@Valid @RequestBody UserDto dto) {
        UserDto result = service.save(dto);
        addOrderLinks(result.getOrders());
        return ResponseEntity.status(HttpStatus.CREATED).body(EntityModel.of(result,
                linkTo(methodOn(UserController.class).findById(null, result.getId()))
                        .withRel(env.getProperty("link.user.current"))
                        .withType(HttpMethod.GET.name()),
                linkTo(methodOn(UserController.class).create(result))
                        .withSelfRel()
                        .withType(HttpMethod.POST.name())));
    }

    private void addOrderLinks(List<OrderDto> orders) {
        orders.forEach(order -> {
            order.getCertificates().forEach(certificate -> {
                addCertificateLinks(certificate);
                addTagLinks(certificate.getTags());
            });
        });
    }

    private void addCertificateLinks(CertificateDto certificate) {
        certificate.add(linkTo(methodOn(CertificateController.class).findById(certificate.getId()))
                .withRel(env.getProperty("link.certificate.current"))
                .withType(HttpMethod.GET.name()));
    }

    private void addTagLinks(Set<TagDto> tags) {
        tags.forEach((tag -> tag.add(linkTo(methodOn(TagController.class).findById(tag.getId()))
                .withRel(env.getProperty("link.tag.current"))
                .withType(HttpMethod.GET.name()))));
    }

    private UserDto createUserWithPaginationInfo(Map<String, String> parameterMap, PageParameter pageParameter) {
        UserDto lastUser = new UserDto();
        if (pageParameter.getPage() >= 2) {
            lastUser.add(linkTo(methodOn(UserController.class).findAll(getFirstPageParameters(parameterMap, pageParameter), getFirstPage(pageParameter)))
                    .withRel(env.getProperty("page.first"))
                    .withType(HttpMethod.GET.name()));
        }
        if (pageParameter.getPreviousPage() >= 2 && pageParameter.getNextPage() < pageParameter.getTotalPageCount()) {
            lastUser.add(linkTo(methodOn(UserController.class).findAll(getPreviousPageParameters(parameterMap, pageParameter), getPreviousPage(pageParameter)))
                    .withRel(env.getProperty("page.previous"))
                    .withType(HttpMethod.GET.name()));
        }
        lastUser.add(linkTo(methodOn(UserController.class).findAll(parameterMap, pageParameter))
                .withRel(env.getProperty("page.current"))
                .withType(HttpMethod.GET.name()));
        if (pageParameter.getNextPage() < pageParameter.getTotalPageCount()) {
            lastUser.add(linkTo(methodOn(UserController.class).findAll(getNextPageParameters(parameterMap, pageParameter), getNextPage(pageParameter)))
                    .withRel(env.getProperty("page.next"))
                    .withType(HttpMethod.GET.name()));
        }
        if (pageParameter.getTotalPageCount() > (pageParameter.getPage())) {
            lastUser.add(linkTo(methodOn(UserController.class).findAll(getLastPageParameters(parameterMap, pageParameter), getLastPage(pageParameter)))
                    .withRel(env.getProperty("page.last"))
                    .withType(HttpMethod.GET.name()));
        }
        return lastUser;
    }
}