DROP DATABASE IF EXISTS gift_service;
CREATE DATABASE gift_service;

Drop Table if exists public.tags;
Create table public.tags
(
    tag_id BIGINT primary key,
    name   text not null,
    constraint unique_tag_name Unique (name)
);
Create
sequence tags_seq
    start 1
    increment 1
    no maxvalue
    cache 1;
Alter table tags
    alter column tag_id set default nextval('tags_seq');

Drop Table if exists public.gift_certificates;
Create table public.gift_certificates
(
    certificate_id   BIGINT primary key,
    name             text    not null,
    description      text    not null,
    price            numeric not null,
    duration         int     not null,
    create_date      timestamp with time zone,
    last_update_date timestamp with time zone
);
Create
sequence gift_certificates_seq
    start 1
    increment 1
    no maxvalue
    cache 1;
Alter table gift_certificates
    alter column certificate_id set default nextval('gift_certificates_seq');

Drop Table if exists public.certificates_tags;
create table public.certificates_tags
(
    certificate_id bigint not null,
    tag_id         bigint not null,

    constraint id_pair_unique unique (certificate_id, tag_id),

    constraint fk_cert_id
        foreign key (certificate_id) references gift_certificates (certificate_id),

    constraint fk_tag_id
        foreign key (tag_id) references tags (tag_id)
);

Drop Table if exists public.users;
Create table public.users
(
    user_id bigint primary key,
    name    text not null,
    constraint unique_user_name Unique (name)
);
Create
sequence users_seq
    start 1
    increment 1
    no maxvalue
    cache 1;
Alter table users
    alter column user_id set default nextval('users_seq');

Drop Table if exists public.orders;
Create table public.orders
(
    order_id      bigint primary key,
    user_id       bigint  not null,
    price         numeric not null,
    purchase_date timestamp with time zone,

    constraint fk_user_id foreign key (user_id) references users (user_id)
);
Create
sequence orders_seq
    start 1
    increment 1
    no maxvalue
    cache 1;
Alter table orders
    alter column order_id set default nextval('orders_seq');

Drop Table if exists public.orders_description;
create table public.orders_description
(
    order_id       bigint not null,
    certificate_id bigint not null,

    constraint fk_order_id
        foreign key (order_id) references orders (order_id),

    constraint orders_description_certificate_id_fkey
        foreign key (certificate_id) references gift_certificates (certificate_id)
);

CREATE OR REPLACE FUNCTION insert_creation_date() RETURNS trigger AS $insert_date$
BEGIN NEW."create_date" = CURRENT_TIMESTAMP;
NEW."last_update_date" = CURRENT_TIMESTAMP;
return NEW;
END;
$insert_date$
LANGUAGE plpgsql;

CREATE TRIGGER insert_creation_date
    BEFORE INSERT
    ON gift_certificates
    FOR EACH ROW
    EXECUTE PROCEDURE insert_creation_date();

CREATE OR REPLACE FUNCTION insert_update_date() RETURNS trigger AS $insert_update_date$
BEGIN NEW."last_update_date" = CURRENT_TIMESTAMP;
return NEW;
END;
$insert_update_date$
LANGUAGE plpgsql;

CREATE TRIGGER insert_update_date
    BEFORE UPDATE
    ON gift_certificates
    FOR EACH ROW
    EXECUTE PROCEDURE insert_update_date();

CREATE OR REPLACE FUNCTION insert_order_creation_date() RETURNS trigger AS $insert_order_date$
BEGIN NEW."purchase_date" = CURRENT_TIMESTAMP;
return NEW;
END;
$insert_order_date$
LANGUAGE plpgsql;

CREATE TRIGGER insert_order_creation_date
    BEFORE INSERT
    ON orders
    FOR EACH ROW
    EXECUTE PROCEDURE insert_order_creation_date();