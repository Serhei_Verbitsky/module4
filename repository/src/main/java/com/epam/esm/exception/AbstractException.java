package com.epam.esm.exception;


@SuppressWarnings("serial")
public class AbstractException extends RuntimeException {
    private final String[] errorDetails;
    private final OperationError operationError;

    public AbstractException(OperationError operationError, String... errorDetails) {
        this.operationError = operationError;
        this.errorDetails = errorDetails;
    }

    public AbstractException(OperationError operationError, String[] errorDetails, Throwable cause) {
        super(cause);
        this.errorDetails = errorDetails;
        this.operationError = operationError;
    }

    public String[] getErrorDetails() {
        return errorDetails;
    }

    public OperationError getOperationError() {
        return operationError;
    }
}