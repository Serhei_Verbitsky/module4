package com.epam.esm.exception;

@SuppressWarnings("serial")
public class RepositoryException extends AbstractException {

    public RepositoryException(OperationError operationError, String... details) {
        super(operationError, details);
    }
}