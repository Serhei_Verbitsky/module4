package com.epam.esm.exception;

public enum OperationError {

    //security codes
    NOT_AUTHORIZED ("40101"),
    ACCESS_DENIED ("40301"),

    //not found codes
    TAG_NOT_FOUND("40401"),
    CERTIFICATE_NOT_FOUND("40402"),
    USER_NOT_FOUND("40403"),
    ORDER_NOT_FOUND("40404"),
    MOST_USED_TAG_NOT_FOUND("40405"),
    USER_NOT_FOUND_BY_NAME("40406"),
    ROLE_NOT_FOUND("40407"),

    //bad request codes
    USER_ALREADY_EXISTS("40012"),
    BAD_REQUEST_PARAMETER("40040"),
    BAD_REQUEST_PARAMETER_ORDER_BY("40030"),
    BAD_REQUEST_MULTIPLE_PARAMETERS_ERROR("40032"),
    BAD_REQUEST_WRONG_CREDENTIALS("40033"),

    DELETE_USED_TAG_ERROR ("40901"),
    DELETE_USED_CERTIFICATE_ERROR ("40902");

    OperationError(String errorCode) {
        this.errorCode = errorCode;
    }

    private final String errorCode;

    public String getErrorCode() {
        return errorCode;
    }
}