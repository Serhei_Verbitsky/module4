package com.epam.esm.repository.impl;

import com.epam.esm.entity.Role;
import com.epam.esm.querygenerator.QueryGenerator;
import com.epam.esm.repository.RoleRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class RoleRepositoryImpl extends AbstractRepository<Role> implements RoleRepository {
    public RoleRepositoryImpl(EntityManager entityManager, QueryGenerator<Role> queryGenerator) {
        super(entityManager, queryGenerator);
    }

    @Override
    protected Class<Role> getEntityClass() {
        return Role.class;
    }

    @Override
    public void update(Role entity) {
        throw new UnsupportedOperationException();
    }
}