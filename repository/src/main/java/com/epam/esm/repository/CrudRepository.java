package com.epam.esm.repository;

import com.epam.esm.pageparameter.PageParameter;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface CrudRepository<E> {
    E save(E entity);

    void delete(E entity);

    Optional<E> findById(Long id);

    void update(E entity);

    List<E> findAll(Map<String, String> parameters, PageParameter pageParameter);

    Integer calculateEntityCount(Map<String, String> parameters, PageParameter pageParameter);
}