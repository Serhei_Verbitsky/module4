package com.epam.esm.repository.impl;

import com.epam.esm.entity.User;
import com.epam.esm.querygenerator.impl.UserQueryGenerator;
import com.epam.esm.repository.UserRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {

    public UserRepositoryImpl(EntityManager entityManager, UserQueryGenerator queryGenerator) {
        super(entityManager, queryGenerator);
    }

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    public void update(User entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(User entity) {
        throw new UnsupportedOperationException();
    }
}