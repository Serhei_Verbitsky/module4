package com.epam.esm.repository.impl;

public enum ParameterType {
    TAG, NAME, DESCRIPTION, SORT_BY_NAME, SORT_BY_DATE;

    public String getParamName() {
        return this.name().toLowerCase();
    }
}
