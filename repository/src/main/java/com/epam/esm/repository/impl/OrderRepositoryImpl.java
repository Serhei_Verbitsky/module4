package com.epam.esm.repository.impl;

import com.epam.esm.entity.Order;
import com.epam.esm.querygenerator.impl.OrderQueryGenerator;
import com.epam.esm.repository.OrderRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class OrderRepositoryImpl extends AbstractRepository<Order> implements OrderRepository {

    public OrderRepositoryImpl(EntityManager entityManager, OrderQueryGenerator queryGenerator) {
        super(entityManager, queryGenerator);
    }

    @Override
    public void update(Order entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Order entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected Class<Order> getEntityClass() {
        return Order.class;
    }
}