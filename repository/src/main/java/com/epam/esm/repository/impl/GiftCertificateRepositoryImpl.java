package com.epam.esm.repository.impl;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.querygenerator.impl.CertificateQueryGenerator;
import com.epam.esm.repository.GiftCertificateRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class GiftCertificateRepositoryImpl extends AbstractRepository<GiftCertificate> implements GiftCertificateRepository {

    public GiftCertificateRepositoryImpl(EntityManager entityManager, CertificateQueryGenerator queryGenerator) {
        super(entityManager, queryGenerator);
    }

    @Override
    protected Class<GiftCertificate> getEntityClass() {
        return GiftCertificate.class;
    }
}