package com.epam.esm.repository.impl;

import com.epam.esm.entity.Tag;
import com.epam.esm.querygenerator.impl.TagQueryGenerator;
import com.epam.esm.repository.TagRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class TagRepositoryImpl extends AbstractRepository<Tag> implements TagRepository {
    public static final String SELECT_MOST_USED_TAG =
            "select t.tag_id, t.name " +
            "from tags t" +
            "         join certificates_tags ct on t.tag_id = ct.tag_id" +
            "         join orders_description od on ct.certificate_id = od.certificate_id" +
            "         join orders o on od.order_id = o.order_id " +
            "where o.user_id = (select user_id" +
            "                   from (select user_id, sum(price) summ" +
            "                         from orders" +
            "                         group by user_id" +
            "                         order by summ desc" +
            "                         limit 1) as uis)" +
            "                   group by t.tag_id, user_id" +
            "                   order by count(t.tag_id) desc" +
            "                   limit 1; ";
    
    public TagRepositoryImpl(EntityManager entityManager, TagQueryGenerator queryGenerator) {
        super(entityManager, queryGenerator);
    }

    @Override
    protected Class<Tag> getEntityClass() {
        return Tag.class;
    }

    @Override
    public void update(Tag entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Tag findUserMostUsedTag() {
        return (Tag) entityManager.createNativeQuery(SELECT_MOST_USED_TAG, Tag.class).getSingleResult();
    }
}