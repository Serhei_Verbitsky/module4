package com.epam.esm.repository.impl;

import com.epam.esm.entity.AbstractEntity;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.querygenerator.QueryGenerator;
import com.epam.esm.repository.CrudRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements CrudRepository<E> {
    protected final EntityManager entityManager;
    protected final CriteriaBuilder criteriaBuilder;
    protected final QueryGenerator<E> queryGenerator;

    protected abstract Class<E> getEntityClass();

    public AbstractRepository(EntityManager entityManager, QueryGenerator<E> queryGenerator) {
        this.queryGenerator = queryGenerator;
        this.entityManager = entityManager;
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public E save(E entity) {
        entityManager.persist(entity);
        entityManager.flush();
        entityManager.refresh(entity);
        return entity;
    }

    @Override
    public void delete(E entity) {
        entityManager.remove(entity);
        entityManager.flush();
    }

    @Override
    public Optional<E> findById(Long id) {
        return Optional.ofNullable(entityManager.find(getEntityClass(), id));
    }

    public List<E> findAll(Map<String, String> parameters, PageParameter pageParameter) {
        return entityManager.createQuery(queryGenerator.getFindAll(parameters, pageParameter, criteriaBuilder).distinct(true))
                .setFirstResult((pageParameter.getPage() * pageParameter.getLimit()) - pageParameter.getLimit())
                .setMaxResults(pageParameter.getLimit())
                .getResultList();
    }

    @Override
    public Integer calculateEntityCount(Map<String, String> parameters, PageParameter pageParameter) {
        TypedQuery<E> query = entityManager.createQuery(queryGenerator.getFindAll(parameters, pageParameter, criteriaBuilder).distinct(true));
        return (int) Math.ceil(query.getResultList().size() / (double) pageParameter.getLimit());
    }

    @Override
    public void update(E entity) {
        entityManager.merge(entity);
    }

    public Optional<E> findByFieldName(String fieldName, String value) {
        try {
            return Optional.ofNullable(entityManager.createQuery(
                    queryGenerator.getFindByFieldName(fieldName, value, criteriaBuilder)).getSingleResult());
        } catch (NoResultException | NonUniqueResultException exception) {
            return Optional.empty();
        }
    }
}