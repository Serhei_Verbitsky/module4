package com.epam.esm.querygenerator.impl;

import com.epam.esm.entity.Order;
import com.epam.esm.predicategenerator.impl.EmptyPredicateGenerator;
import org.springframework.stereotype.Component;

@Component
public class OrderQueryGenerator extends AbstractQueryGenerator<Order> {
    public OrderQueryGenerator(EmptyPredicateGenerator<Order> predicateGenerator) {
        super(predicateGenerator);
    }

    @Override
    public Class<Order> getEntityClass() {
        return Order.class;
    }
}