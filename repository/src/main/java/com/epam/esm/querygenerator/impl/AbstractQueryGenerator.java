package com.epam.esm.querygenerator.impl;

import com.epam.esm.entity.AbstractEntity;
import com.epam.esm.entity.AvailableSorting;
import com.epam.esm.exception.OperationError;
import com.epam.esm.exception.RepositoryException;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.predicategenerator.PredicateGenerator;
import com.epam.esm.querygenerator.QueryGenerator;
import org.hibernate.query.criteria.internal.OrderImpl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractQueryGenerator<E extends AbstractEntity> implements QueryGenerator<E> {
    private final PredicateGenerator<E> predicateGenerator;
    private static final String ASC_ORDER = "asc";
    private static final String ORDER_DELIMITER = "\\.";
    private final Set<String> orderedFields;

    public abstract Class<E> getEntityClass();

    public AbstractQueryGenerator(PredicateGenerator<E> predicateGenerator) {
        this.predicateGenerator = predicateGenerator;
        this.orderedFields = getOrderedFields();
    }

    @Override
    public CriteriaQuery<E> getFindAll(Map<String, String> parameters, PageParameter pageParameter, CriteriaBuilder criteriaBuilder) {
        CriteriaQuery<E> criteria = criteriaBuilder.createQuery(getEntityClass());
        Root<E> root = criteria.from(getEntityClass());
        criteria.select(root);
        Optional<Predicate> predicateByParameters = predicateGenerator.generateFindAllPredicate(parameters, root, criteriaBuilder);
        predicateByParameters.ifPresent(criteria::where);
        generateOrderParameter(pageParameter, root, criteria);
        return criteria;
    }

    @Override
    public CriteriaQuery<E> getFindByFieldName(String fieldName, String value, CriteriaBuilder criteriaBuilder) {
        CriteriaQuery<E> criteria = criteriaBuilder.createQuery(getEntityClass());
        Root<E> root = criteria.from(getEntityClass());
        criteria.select(root).where(criteriaBuilder.equal(root.get(fieldName), value));
        return criteria;
    }

    @Override
    public void generateOrderParameter(PageParameter pageParameter, Root<E> root, CriteriaQuery<E> criteriaQuery) {
        OrderImpl idOrdering = new OrderImpl(root.get("id"), true);
        List<String> orderParameters = pageParameter.getOrderBy();
        List<Order> orderList = new ArrayList<>();
        if (orderParameters != null && !orderParameters.isEmpty()) {
            for (String order : orderParameters) {
                String[] orderString = order.split(ORDER_DELIMITER);
                if (orderedFields.contains(orderString[0])) {
                    if (orderString[1].equalsIgnoreCase(ASC_ORDER)) {
                        orderList.add(new OrderImpl(root.get(orderString[0]), true));
                    } else {
                        orderList.add(new OrderImpl(root.get(orderString[0]), false));
                    }
                } else {
                    throw new RepositoryException(OperationError.BAD_REQUEST_PARAMETER_ORDER_BY, orderString[0]);
                }
            }
            orderList.add(idOrdering);
            criteriaQuery.orderBy(orderList);
        }else {
            criteriaQuery.orderBy(idOrdering);
        }
    }

    private Set<String> getOrderedFields() {
        Field[] classFields = getEntityClass().getDeclaredFields();
        return Arrays.stream(classFields)
                .filter(field -> field.getAnnotation(AvailableSorting.class) != null)
                .map(Field::getName)
                .collect(Collectors.toSet());
    }
}