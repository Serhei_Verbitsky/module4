package com.epam.esm.querygenerator.impl;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.predicategenerator.impl.CertificatePredicateGenerator;
import org.springframework.stereotype.Component;

@Component
public class CertificateQueryGenerator extends AbstractQueryGenerator<GiftCertificate> {
    public CertificateQueryGenerator(CertificatePredicateGenerator predicateGenerator) {
        super(predicateGenerator);
    }

    @Override
    public Class<GiftCertificate> getEntityClass() {
        return GiftCertificate.class;
    }
}