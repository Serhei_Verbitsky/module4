package com.epam.esm.querygenerator.impl;

import com.epam.esm.entity.User;
import com.epam.esm.predicategenerator.impl.EmptyPredicateGenerator;
import org.springframework.stereotype.Component;

@Component
public class UserQueryGenerator extends AbstractQueryGenerator<User> {
    public UserQueryGenerator(EmptyPredicateGenerator<User> predicateGenerator) {
        super(predicateGenerator);
    }

    @Override
    public Class<User> getEntityClass() {
        return User.class;
    }
}