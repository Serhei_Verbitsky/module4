package com.epam.esm.querygenerator;

import com.epam.esm.entity.AbstractEntity;
import com.epam.esm.pageparameter.PageParameter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Map;

public interface QueryGenerator<E extends AbstractEntity> {
    CriteriaQuery<E> getFindAll(Map<String, String> parameters, PageParameter pageParameter, CriteriaBuilder criteriaBuilder);

    CriteriaQuery<E> getFindByFieldName(String fieldName, String value, CriteriaBuilder criteriaBuilder);

    void generateOrderParameter(PageParameter pageParameter, Root<E> root, CriteriaQuery<E> criteriaQuery);
}