package com.epam.esm.querygenerator.impl;

import com.epam.esm.entity.Tag;
import com.epam.esm.predicategenerator.impl.EmptyPredicateGenerator;
import org.springframework.stereotype.Component;

@Component
public class TagQueryGenerator extends AbstractQueryGenerator<Tag> {
    public TagQueryGenerator(EmptyPredicateGenerator<Tag> predicateGenerator) {
        super(predicateGenerator);
    }

    @Override
    public Class<Tag> getEntityClass() {
        return Tag.class;
    }
}