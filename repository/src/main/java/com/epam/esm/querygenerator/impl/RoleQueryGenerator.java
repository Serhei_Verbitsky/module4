package com.epam.esm.querygenerator.impl;

import com.epam.esm.entity.Role;
import com.epam.esm.predicategenerator.impl.EmptyPredicateGenerator;
import org.springframework.stereotype.Component;

@Component
public class RoleQueryGenerator extends AbstractQueryGenerator<Role> {
    public RoleQueryGenerator(EmptyPredicateGenerator<Role> predicateGenerator) {
        super(predicateGenerator);
    }

    @Override
    public Class<Role> getEntityClass() {
        return Role.class;
    }
}