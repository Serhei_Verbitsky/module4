package com.epam.esm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Audited
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "roles")
@Data
@AttributeOverride(name = "id", column = @Column(name = "role_id"))
@JsonIgnoreProperties("id")
@AllArgsConstructor
@NoArgsConstructor
public class Role extends AbstractEntity{
    private String name;
}