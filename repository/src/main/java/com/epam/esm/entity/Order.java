package com.epam.esm.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Audited
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = "orders")
@AttributeOverride(name = "id", column = @Column(name = "order_id"))
@ToString(exclude = {"user", "certificates"})
public class Order extends AbstractEntity {

    @Column(name = "price")
    private BigDecimal orderPrice;

    @Column(name = "purchase_date", insertable = false, updatable = false)
    private LocalDateTime purchaseDate;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "orders_description",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "certificate_id"))
    private List<GiftCertificate> certificates;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}