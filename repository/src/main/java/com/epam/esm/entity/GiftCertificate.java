package com.epam.esm.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Audited
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = "gift_certificates")
@AttributeOverride(name = "id", column = @Column(name = "certificate_id"))
@ToString(exclude = "tags")
public class GiftCertificate extends AbstractEntity {

    @AvailableSorting
    private String name;

    @AvailableSorting
    private String description;

    @AvailableSorting
    private BigDecimal price;

    @AvailableSorting
    private Integer duration;

    @Column(name = "create_date", insertable = false, updatable = false)
    @AvailableSorting
    private LocalDateTime createDate;

    @Column(name = "last_update_date", insertable = false, updatable = false)
    private LocalDateTime lastUpdateDate;

    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(name = "certificates_tags",
            joinColumns = @JoinColumn(name = "certificate_id"),
            inverseJoinColumns = @JoinColumn (name = "tag_id"))
    private Set<Tag> tags;
}