package com.epam.esm.pageparameter;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Data
@Validated
public class PageParameter {
    private static final Integer DEFAULT_PAGE = 1;
    private static final Integer DEFAULT_LIMIT = 10;
    @Min(value = 1, message = "40001")
    private Integer page;
    @Min(value = 1, message = "40002")
    private Integer limit;
    private List<@Valid @Pattern(regexp = "[\\w]+\\.(asc|desc)", message = "40031") String> orderBy;
    private Integer totalPageCount;

    public PageParameter(Integer page, Integer limit, List<String> orderList) {
        this.page = (page != null && page > 0) ? page : DEFAULT_PAGE;
        this.limit = (limit != null && limit > 0) ? limit : DEFAULT_LIMIT;
        this.orderBy = (orderList != null) ? orderList : new ArrayList<>();
    }

    public Integer getNextPage() {
        return page < totalPageCount ? page + 1 : totalPageCount;
    }

    public Integer getPreviousPage() {
        return page == 1 ? 1 : page - 1;
    }
}