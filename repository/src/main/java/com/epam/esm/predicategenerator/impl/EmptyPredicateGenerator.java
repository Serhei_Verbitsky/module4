package com.epam.esm.predicategenerator.impl;

import com.epam.esm.predicategenerator.PredicateGenerator;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;
import java.util.Optional;

@Component
public class EmptyPredicateGenerator<E> implements PredicateGenerator<E> {
    @Override
    public Optional<Predicate> generateFindAllPredicate(Map<String, String> parameters, Root<E> rootClass, CriteriaBuilder criteriaBuilder) {
        return Optional.empty();
    }
}