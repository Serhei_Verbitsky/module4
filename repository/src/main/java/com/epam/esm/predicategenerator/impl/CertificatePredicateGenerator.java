package com.epam.esm.predicategenerator.impl;

import com.epam.esm.entity.AbstractEntity;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.GiftCertificate_;
import com.epam.esm.entity.Tag_;
import com.epam.esm.predicategenerator.PredicateGenerator;
import com.epam.esm.repository.impl.ParameterType;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CertificatePredicateGenerator implements PredicateGenerator<GiftCertificate> {

    private static final String SQL_WILDCARD = "%";

    public CertificatePredicateGenerator() {
        super();
    }

    private static String wrapParameter(String parameter) {
        return SQL_WILDCARD.concat(parameter).concat(SQL_WILDCARD);
    }

    @Override
    public Optional<Predicate> generateFindAllPredicate(
            Map<String, String> parameters, Root<GiftCertificate> root, CriteriaBuilder criteriaBuilder) {

        CriteriaBuilder.Coalesce<String> nameCoalesce =
                buildCoalesce(parameters.get(ParameterType.NAME.getParamName()), criteriaBuilder);
        CriteriaBuilder.Coalesce<String> descriptionCoalesce =
                buildCoalesce(parameters.get(ParameterType.DESCRIPTION.getParamName()), criteriaBuilder);

        Predicate namePredicate = criteriaBuilder.like(
                criteriaBuilder.lower(root.get(GiftCertificate_.NAME)), criteriaBuilder.lower(nameCoalesce));
        Predicate descriptionPredicate = criteriaBuilder.like(
                criteriaBuilder.lower(root.get(GiftCertificate_.DESCRIPTION)), criteriaBuilder.lower(descriptionCoalesce));

        String tags = parameters.get(ParameterType.TAG.getParamName());
        if (tags != null && tags.matches("(([\\u0410-\\u044f\\w\\d\\s?!._'\\-])+(,)?)*")) {
            Predicate tagPredicate = createTagPredicate(parameters.get(ParameterType.TAG.getParamName()), criteriaBuilder, root);
            return Optional.of(criteriaBuilder.and(namePredicate, descriptionPredicate, tagPredicate));
        } else {
            return Optional.of(criteriaBuilder.and(namePredicate, descriptionPredicate));
        }
    }

    private Predicate createTagPredicate(String tags, CriteriaBuilder criteriaBuilder,
                                         Root<? extends AbstractEntity> root) {
        String[] tagArray = tags.split("\\s*,\\s*");
        List<Predicate> tagPredicates = Arrays.stream(tagArray)
                .map(tag ->
                        criteriaBuilder.like(
                                criteriaBuilder.lower(
                                        root.join(GiftCertificate_.TAGS).get(Tag_.NAME)), tag.toLowerCase()))
                .collect(Collectors.toList());
        return criteriaBuilder.and(tagPredicates.toArray(tagPredicates.toArray(new Predicate[tagArray.length])));
    }

    private CriteriaBuilder.Coalesce<String> buildCoalesce(String parameter, CriteriaBuilder cb) {
        CriteriaBuilder.Coalesce<String> coalesce = cb.coalesce();
        if (parameter != null) {
            coalesce.value(wrapParameter(parameter));
        }
        coalesce.value(SQL_WILDCARD);
        return coalesce;
    }
}