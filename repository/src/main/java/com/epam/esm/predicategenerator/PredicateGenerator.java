package com.epam.esm.predicategenerator;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Map;
import java.util.Optional;

public interface PredicateGenerator<E> {
    Optional<Predicate> generateFindAllPredicate(Map<String, String> parameters, Root<E> rootClass, CriteriaBuilder criteriaBuilder);
}