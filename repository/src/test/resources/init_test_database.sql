create table tags
(
    tag_id bigint auto_increment
        primary key,
    name   varchar(50) not null,

    constraint tag_name_unique unique (name)

);

create table gift_certificates
(
    certificate_id   bigint auto_increment              primary key,
    name             varchar(50)                        not null,
    description      varchar(250)                       not null,
    price            double                             not null,
    duration         int                                not null,
    create_date      timestamp                          null,
    last_update_date timestamp                          null
);

create table certificates_tags
(
    id              bigint auto_increment primary key,
    certificate_id  bigint not null,
    tag_id          bigint not null,

    constraint certificates_tags_ibfk_1
        foreign key (certificate_id) references gift_certificates (certificate_id),
    constraint certificates_tags_ibfk_2
        foreign key (tag_id) references tags (tag_id)
);

create table roles
(
    role_id         bigint auto_increment               primary key,
    name            varchar(50)                         null,

    constraint unique_role_name unique (name)
);

create table users
(
    user_id         bigint auto_increment               primary key,
    name            varchar(50)                         null,
    password        varchar(70)                         null,
    role_id         bigint                              not null,

    constraint fk_user_roles
        foreign key (role_id) references roles (role_id),
    constraint unique_user_name unique (name)
);

create table orders
(
    order_id        bigint auto_increment               primary key,
    user_id         bigint not null                     not null,
    price           double                              null,
    purchase_date   timestamp                           null,

    constraint orders_users_userId_ibfk_1
        foreign key (user_id) references users (user_id)
);

create table orders_description
(
    id              bigint auto_increment primary key,
    order_id        bigint not null,
    certificate_id  bigint not null,

    constraint order_id_ibfk_1
        foreign key (order_id) references orders (order_id),
    constraint certificate_id_ibfk_1
        foreign key (certificate_id) references gift_certificates (certificate_id)
);

create index certificate_id
    on certificates_tags (certificate_id);

create index tag_id
    on certificates_tags (tag_id);

create index user_id
    on users (user_id);

create index role_id
    on roles (role_id);

create index order_id
    on orders (order_id);