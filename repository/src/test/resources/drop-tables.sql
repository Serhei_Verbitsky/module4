drop table if exists certificates_tags;
drop table if exists orders_description;
drop table if exists orders;
drop table if exists gift_certificates;
drop table if exists tags;
drop table if exists users;
drop table if exists roles;
