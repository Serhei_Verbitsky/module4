INSERT INTO gift_certificates (certificate_id, name, description, price, duration, create_date, last_update_date)
VALUES (1, 'certificate one', 'description', 10, 1, '2021-01-21 22:39:57.032389', '2021-01-21 22:39:57.032389'),
       (2, 'certificate two', 'some text', 20, 2, '2021-01-21 22:39:57.032389', '2021-01-21 22:39:57.032389'),
       (3, 'certificate three', 'third row', 30, 3, '2021-01-21 22:39:57.032389', '2021-01-21 22:39:57.032389'),
       (4, 'certificate four', 'fourth row', 40, 4, '2021-01-21 22:39:57.032389', '2021-01-21 22:39:57.032389');

INSERT INTO tags (tag_id, name)
VALUES (1, 'tag one'),
       (2, 'tag two'),
       (3, 'tag three'),
       (4, 'fourth tag'),
       (5, 'fifth tag'),
       (6, 'tag six'),
       (7, 'deleted');

INSERT INTO certificates_tags (certificate_id, tag_id)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (2, 3),
       (2, 4);

INSERT INTO roles (role_id, name)
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_USER');

INSERT INTO users (user_id, name, password, role_id)
VALUES (1, 'user1', 'pass1', 1),
       (2, 'user2', 'pass1', 2),
       (3, 'user3', 'pass1', 2);

INSERT INTO orders (order_id, user_id, price, purchase_date)
VALUES (1, 1, 10, '2021-01-21 22:39:57.032389'),
       (2, 1, 50, '2021-01-21 22:39:57.032389'),
       (3, 2, 40, '2021-01-21 22:39:57.032389');

INSERT INTO orders_description (order_id, certificate_id)
VALUES (1, 1),
       (2, 2),
       (2, 3),
       (3, 3)