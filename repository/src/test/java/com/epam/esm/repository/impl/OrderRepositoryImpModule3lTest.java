package com.epam.esm.repository.impl;

import com.epam.esm.entity.Order;
import com.epam.esm.entity.User;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.OrderRepository;
import com.epam.esm.repository.config.TestRepositoryConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringJUnitConfig(TestRepositoryConfig.class)
@ActiveProfiles("test")
@Sql({"classpath:drop-tables.sql",
        "classpath:init_test_database.sql",
        "classpath:fill_test_data.sql"})
class OrderRepositoryImpModule3lTest {
    @Autowired
    private OrderRepository repository;

    private Map<String, String> parameters;

    @BeforeEach
    void init() {
        parameters = new HashMap<>();
    }

    @Test
    void testSavePositive() {
        Order order = new Order();
        order.setCertificates(new ArrayList<>());
        order.setOrderPrice(new BigDecimal(10));
        order.setPurchaseDate(LocalDateTime.now());
        User user = new User();
        user.setOrders(new ArrayList<>());
        user.setId(1L);
        user.setUserName("user1");
        order.setUser(user);
        Order actual = repository.save(order);
        assertTrue(actual.getId() > 0);
    }

    @Test
    void testFindByIdPositive() {
        Optional<Order> order = repository.findById(1L);
        assertTrue(order.isPresent());
    }

    @Test
    void testFindByIdNegativeNotFound() {
        Optional<Order> order = repository.findById(10L);
        assertFalse(order.isPresent());
    }

    @Test
    void testFindAllPositive() {
        PageParameter pageParameterTest = new PageParameter(1, 3, new ArrayList<>());
        List<Order> orders = repository.findAll(parameters, pageParameterTest);
        assertEquals(orders.size(), pageParameterTest.getLimit());
    }

    @Test
    void testUpdateShouldThrowsRepositoryException() {
        assertThrows(UnsupportedOperationException.class, () -> repository.update(new Order()));
    }

    @Test
    void testDeleteShouldThrowsRepositoryException() {
        assertThrows(UnsupportedOperationException.class, () -> repository.delete(new Order()));
    }
}