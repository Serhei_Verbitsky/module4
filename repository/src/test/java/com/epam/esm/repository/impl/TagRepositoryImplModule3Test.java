package com.epam.esm.repository.impl;

import com.epam.esm.entity.Tag;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.config.TestRepositoryConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringJUnitConfig(TestRepositoryConfig.class)
@ActiveProfiles("test")
@Sql({"classpath:drop-tables.sql",
        "classpath:init_test_database.sql",
        "classpath:fill_test_data.sql"})
class TagRepositoryImplModule3Test {

    @Autowired
    private TagRepository repository;

    private PageParameter pageParameter;
    private Map<String, String> parameters;

    @BeforeEach
    void init() {
        pageParameter = new PageParameter(1, 100, new ArrayList<>());
        parameters = new HashMap<>();
    }

    @Test
    void testSavePositive() {
        Tag tag = new Tag();
        tag.setName("tag1");
        Tag actual = repository.save(tag);
        assertTrue(actual.getId() > 0);
    }

    @Test
    void testDeletePositive() {
        List<Tag> allTags = repository.findAll(parameters, pageParameter);
        Tag tag = allTags.get(allTags.size() - 1);
        repository.delete(tag);
        List<Tag> tagsAfterDelete = repository.findAll(parameters, pageParameter);
        assertFalse(tagsAfterDelete.contains(tag));
    }

    @Test
    void testFindByIdPositive() {
        Optional<Tag> tag = repository.findById(1L);
        assertTrue(tag.isPresent());
    }

    @Test
    void testFindByIdNegativeNotFound() {
        Optional<Tag> tag = repository.findById(10L);
        assertFalse(tag.isPresent());
    }

    @Test
    void testFindAllPositive() {
        PageParameter pageParameterTest = new PageParameter(1, 5, new ArrayList<>());
        List<Tag> tags = repository.findAll(parameters, pageParameterTest);
        assertEquals(tags.size(), pageParameterTest.getLimit());
    }

    @Test
    void testUpdateShouldThrowsRepositoryException() {
        assertThrows(UnsupportedOperationException.class, () -> repository.update(new Tag()));
    }

    @Test
    void testFindByFieldNamePositive() {
        Optional<Tag> tag = repository.findByFieldName("name", "tag one");
        assertTrue(tag.isPresent());
    }

    @Test
    void testFindByFieldNamePositiveEmptyResult() {
        Optional<Tag> tag = repository.findByFieldName("name", "not found");
        assertFalse(tag.isPresent());
    }

    @Test
    void findUserMostUsedTag() {
        Tag userMostUsedTag = repository.findUserMostUsedTag();
        assertEquals(userMostUsedTag.getName(), "tag three");
    }
}