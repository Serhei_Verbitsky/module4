package com.epam.esm.repository.impl;

import com.epam.esm.entity.Role;
import com.epam.esm.entity.User;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.repository.config.TestRepositoryConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringJUnitConfig(TestRepositoryConfig.class)
@ActiveProfiles("test")
@Sql({"classpath:drop-tables.sql",
        "classpath:init_test_database.sql",
        "classpath:fill_test_data.sql"})
class UserRepositoryImpModule3lTest {
    @Autowired
    private UserRepository repository;

    private Map<String, String> parameters;

    @BeforeEach
    void init() {
        parameters = new HashMap<>();
    }

    @Test
    void testSavePositive() {
        User user = new User();
        user.setOrders(new ArrayList<>());
        user.setUserName("savedUser");
        Role role = new Role();
        role.setName("ROLE_ADMIN");
        role.setId(1L);
        user.setRole(role);
        user.setPassword("password");
        User actual = repository.save(user);
        assertTrue(actual.getId() > 0);
    }

    @Test
    void testFindByIdPositive() {
        Optional<User> user = repository.findById(1L);
        assertTrue(user.isPresent());
    }

    @Test
    void testFindByIdNegativeNotFound() {
        Optional<User> user = repository.findById(10L);
        assertFalse(user.isPresent());
    }

    @Test
    void testFindAllPositive() {
        PageParameter pageParameterTest = new PageParameter(1, 3, new ArrayList<>());
        List<User> users = repository.findAll(parameters, pageParameterTest);
        assertEquals(users.size(), pageParameterTest.getLimit());
    }

    @Test
    void testUpdateShouldThrowsRepositoryException() {
        assertThrows(UnsupportedOperationException.class, () -> repository.update(new User()));
    }

    @Test
    void testDeleteShouldThrowsRepositoryException() {
        assertThrows(UnsupportedOperationException.class, () -> repository.delete(new User()));
    }
}