package com.epam.esm.repository.impl;

import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.repository.config.TestRepositoryConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringJUnitConfig(TestRepositoryConfig.class)
@ActiveProfiles("test")
@Sql({"classpath:drop-tables.sql",
        "classpath:init_test_database.sql",
        "classpath:fill_test_data.sql"})
class GiftCertificateRepositoryImplModule3Test {

    @Autowired
    private GiftCertificateRepository repository;

    private PageParameter pageParameter;
    private Map<String, String> parameters;

    @BeforeEach
    void init() {
        pageParameter = new PageParameter(1, 100, new ArrayList<>());
        parameters = new HashMap<>();
    }

    @Test
    void testSavePositive() {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setName("saved");
        certificate.setDescription("saved");
        certificate.setPrice(new BigDecimal(10));
        certificate.setTags(new HashSet<>());
        certificate.setDuration(10);
        GiftCertificate actual = repository.save(certificate);
        assertTrue(actual.getId() > 0);
    }

    @Test
    void testDeletePositive() {
        List<GiftCertificate> allGiftCertificates = repository.findAll(parameters, pageParameter);
        GiftCertificate deleted = allGiftCertificates.get(allGiftCertificates.size() - 1);
        repository.delete(deleted);
        List<GiftCertificate> GiftCertificatesAfterDelete = repository.findAll(parameters, pageParameter);
        assertFalse(GiftCertificatesAfterDelete.contains(deleted));
    }

    @Test
    void testFindByIdPositive() {
        Optional<GiftCertificate> GiftCertificate = repository.findById(1L);
        assertTrue(GiftCertificate.isPresent());
    }

    @Test
    void testFindByIdPositiveEmptyResult() {
        Optional<GiftCertificate> GiftCertificate = repository.findById(10L);
        assertFalse(GiftCertificate.isPresent());
    }

    @Test
    void testFindAllPositive() {
        PageParameter pageParameterTest = new PageParameter(1, 3, new ArrayList<>());
        List<GiftCertificate> GiftCertificates = repository.findAll(parameters, pageParameterTest);
        assertEquals(GiftCertificates.size(), pageParameterTest.getLimit());
    }

    @Test
    void testUpdatePositive() {
        Optional<GiftCertificate> certificate = repository.findById(1L);
        GiftCertificate expected = certificate.get();
        expected.setName("updated");
        Optional<GiftCertificate> certificate1 = repository.findById(1L);
        GiftCertificate actual = certificate1.get();
        assertEquals(actual, expected);
    }
}