package com.epam.esm.repository.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@ComponentScans({
        @ComponentScan(basePackages = "com.epam.esm.repository"),
        @ComponentScan(basePackages = "com.epam.esm.querygenerator"),
        @ComponentScan(basePackages = "com.epam.esm.predicategenerator")
})
@EntityScan("com.epam.esm.entity")
public class TestRepositoryConfig {
    public static void main(String[] args) {
        SpringApplication.run(TestRepositoryConfig.class, args);
    }
}
