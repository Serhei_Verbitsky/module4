package com.epam.esm.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Order.class)
public abstract class Order_ extends com.epam.esm.entity.AbstractEntity_ {

	public static volatile SingularAttribute<Order, LocalDateTime> purchaseDate;
	public static volatile ListAttribute<Order, GiftCertificate> certificates;
	public static volatile SingularAttribute<Order, BigDecimal> orderPrice;
	public static volatile SingularAttribute<Order, User> user;

	public static final String PURCHASE_DATE = "purchaseDate";
	public static final String CERTIFICATES = "certificates";
	public static final String ORDER_PRICE = "orderPrice";
	public static final String USER = "user";

}

