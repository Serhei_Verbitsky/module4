package com.epam.esm.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tag.class)
public abstract class Tag_ extends com.epam.esm.entity.AbstractEntity_ {

	public static volatile SingularAttribute<Tag, String> name;

	public static final String NAME = "name";

}

