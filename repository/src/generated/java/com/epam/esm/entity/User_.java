package com.epam.esm.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ extends com.epam.esm.entity.AbstractEntity_ {

	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, Role> role;
	public static volatile ListAttribute<User, Order> orders;
	public static volatile SingularAttribute<User, String> userName;

	public static final String PASSWORD = "password";
	public static final String ROLE = "role";
	public static final String ORDERS = "orders";
	public static final String USER_NAME = "userName";

}

