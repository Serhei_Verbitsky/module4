package com.epam.esm.service;

import com.epam.esm.dto.TagDto;

public interface TagService extends BaseService<TagDto> {

    TagDto findUserMostUsedTag();

    TagDto findOrSave(TagDto tagDto);
}