package com.epam.esm.service;

import com.epam.esm.converter.impl.OrderSaveRequest;
import com.epam.esm.dto.OrderDto;
import org.springframework.security.core.Authentication;

public interface OrderService extends BaseService <OrderDto>{
    OrderDto save(OrderSaveRequest orderSaveRequest, Authentication authentication);

    OrderDto findById(Long id, Authentication authentication);
}
