package com.epam.esm.service;

import com.epam.esm.dto.CertificateDto;

public interface GiftCertificateService extends BaseService<CertificateDto> {

    void patch(CertificateDto patchedCertificateDto);
}