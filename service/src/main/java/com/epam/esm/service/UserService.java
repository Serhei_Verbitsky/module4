package com.epam.esm.service;

import com.epam.esm.dto.UserDto;
import org.springframework.security.core.Authentication;

public interface UserService extends BaseService<UserDto> {
    UserDto findByName(String username);

    UserDto findById(Long id, Authentication authentication);
}