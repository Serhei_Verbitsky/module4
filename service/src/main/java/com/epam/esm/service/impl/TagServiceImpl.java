package com.epam.esm.service.impl;

import com.epam.esm.converter.DtoEntityConverter;
import com.epam.esm.dto.TagDto;
import com.epam.esm.entity.Tag;
import com.epam.esm.exception.CertificateServiceException;
import com.epam.esm.exception.OperationError;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.service.TagService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.epam.esm.exception.OperationError.MOST_USED_TAG_NOT_FOUND;
import static com.epam.esm.exception.OperationError.TAG_NOT_FOUND;

@Service
public class TagServiceImpl extends AbstractService<TagDto, Tag> implements TagService {

    public TagServiceImpl(
            TagRepository repository,
            DtoEntityConverter<TagDto, Tag> converter) {
        super(repository, converter);
    }

    @Override
    protected OperationError getNotFoundErrorCode() {
        return TAG_NOT_FOUND;
    }

    @Transactional
    @Override
    public TagDto findOrSave(TagDto tagDto) {
        Optional<Tag> tagByName = ((TagRepository) repository).findByFieldName("name", tagDto.getName());
        return tagByName.map(converter::toDto)
                .orElseGet(() -> {
                    tagDto.setId(null);
                    return converter.toDto(repository.save(converter.toEntity(tagDto)));
                });
    }

    @Override
    public void delete(Long id) {
        try {
            super.delete(id);
        } catch (DataIntegrityViolationException ex) {
            throw new CertificateServiceException(OperationError.DELETE_USED_TAG_ERROR, String.valueOf(id));
        }
    }

    @Override
    public TagDto findUserMostUsedTag() {
        try {
            return converter.toDto(((TagRepository) repository).findUserMostUsedTag());
        }catch (EmptyResultDataAccessException exception) {
            throw new CertificateServiceException(MOST_USED_TAG_NOT_FOUND);
        }
    }

    @Override
    public void update(TagDto entityDto) {
        throw new UnsupportedOperationException();
    }
}