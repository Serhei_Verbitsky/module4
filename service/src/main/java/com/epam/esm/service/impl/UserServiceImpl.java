package com.epam.esm.service.impl;

import com.epam.esm.converter.DtoEntityConverter;
import com.epam.esm.dto.UserDto;
import com.epam.esm.dto.UserRole;
import com.epam.esm.entity.Role;
import com.epam.esm.entity.User;
import com.epam.esm.entity.User_;
import com.epam.esm.exception.CertificateServiceException;
import com.epam.esm.exception.OperationError;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.RoleRepository;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.epam.esm.exception.OperationError.ACCESS_DENIED;
import static com.epam.esm.exception.OperationError.USER_ALREADY_EXISTS;
import static com.epam.esm.exception.OperationError.USER_NOT_FOUND;
import static com.epam.esm.exception.OperationError.USER_NOT_FOUND_BY_NAME;

@Service
public class UserServiceImpl extends AbstractService<UserDto, User> implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private Map<UserRole, Role> userRoles;

    public UserServiceImpl(UserRepository repository, DtoEntityConverter<UserDto, User> converter,
                           UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder passwordEncoder) {
        super(repository, converter);
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        userRoles = new HashMap<>();
    }

    @PostConstruct
    private void init() {
        userRoles = roleRepository.findAll(new HashMap<>(),
                new PageParameter(1, UserRole.values().length, new ArrayList<>()))
                .stream().collect(Collectors.toMap(role -> UserRole.resolve(role.getName()), Function.identity()));
    }

    @Override
    protected OperationError getNotFoundErrorCode() {
        return USER_NOT_FOUND;
    }

    @Transactional
    @Override
    public UserDto save(UserDto userDto) {
        Optional<User> existedUser = userRepository.findByFieldName(User_.USER_NAME, userDto.getUserName());
        if (existedUser.isPresent()) {
            throw new CertificateServiceException(USER_ALREADY_EXISTS, userDto.getUserName());
        }
        User userToSave = converter.toEntity(userDto);
        userToSave.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userToSave.setRole(userRoles.get(UserRole.resolve(UserRole.DEFAULT_ROLE.getName())));
        Long savedId = repository.save(userToSave).getId();
        return repository.findById(savedId)
                .map( user -> converter.toDto(user))
                .orElseThrow(() -> new CertificateServiceException(USER_NOT_FOUND, String.valueOf(savedId)));
    }

    @Transactional(readOnly = true)
    @Override
    public UserDto findByName(String username) {
        Optional<User> userByName = userRepository.findByFieldName(User_.USER_NAME, username);
        return userByName.map(user -> converter.toDto(user))
                .orElseThrow(() -> new CertificateServiceException(USER_NOT_FOUND_BY_NAME, username));
    }

    @Transactional(readOnly = true)
    @Override
    public UserDto findById(Long id, Authentication authentication) {
        String userName = String.valueOf(authentication.getPrincipal());
        Optional<User> requestedUser = repository.findById(id);
        if (requestedUser.isPresent()) {
            if (isUserHasAdminRole(authentication) || userName.equals(requestedUser.get().getUserName())) {
                return converter.toDto(requestedUser.get());
            }
        } else {
            throw new CertificateServiceException(getNotFoundErrorCode(), String.valueOf(id));
        }
        throw new CertificateServiceException(ACCESS_DENIED);
    }

    @Override
    public void update(UserDto entityDto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long id) {
        throw new UnsupportedOperationException();
    }
}