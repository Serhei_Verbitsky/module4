package com.epam.esm.service.impl;

import com.epam.esm.converter.DtoEntityConverter;
import com.epam.esm.dto.CertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.exception.CertificateServiceException;
import com.epam.esm.exception.OperationError;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.service.TagService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.epam.esm.exception.OperationError.CERTIFICATE_NOT_FOUND;

@Service
public class GiftCertificateServiceImpl
        extends AbstractService<CertificateDto, GiftCertificate> implements GiftCertificateService {

    private final TagService tagService;

    public GiftCertificateServiceImpl(GiftCertificateRepository repository,
                                      DtoEntityConverter<CertificateDto, GiftCertificate> converter, TagService tagService) {
        super(repository, converter);
        this.tagService = tagService;
    }

    @Override
    protected OperationError getNotFoundErrorCode() {
        return CERTIFICATE_NOT_FOUND;
    }

    @Transactional
    @Override
    public void update(CertificateDto entityDto) {
        Set<TagDto> addedTags = saveOrUpdateCertificateTags(entityDto);
        entityDto.setTags(addedTags);
        repository.update(converter.toEntity(entityDto));
    }

    @Transactional
    @Override
    public void delete(Long id) {
        try {
            super.delete(id);
        } catch (DataIntegrityViolationException ex) {
            throw new CertificateServiceException(OperationError.DELETE_USED_CERTIFICATE_ERROR, String.valueOf(id));
        }
    }

    @Transactional
    @Override
    public CertificateDto save(CertificateDto entityDto) {
        Set<TagDto> addedTags = saveOrUpdateCertificateTags(entityDto);
        entityDto.setTags(null);
        GiftCertificate certificateToSave = repository.save(converter.toEntity(entityDto));
        entityDto.setId(certificateToSave.getId());
        entityDto.setTags(addedTags);
        repository.update(converter.toEntity(entityDto));
        return (entityDto);
    }

    @Transactional
    @Override
    public void patch(CertificateDto patchedCertificateDto) {
        Optional<GiftCertificate> dbCertificate = repository.findById(patchedCertificateDto.getId());
        GiftCertificate certificateToPatch;
        if (dbCertificate.isPresent()) {
            certificateToPatch = dbCertificate.get();
        }else {
            throw new CertificateServiceException(CERTIFICATE_NOT_FOUND, String.valueOf(patchedCertificateDto.getId()));
        }

        String certificateName = patchedCertificateDto.getName() == null ? certificateToPatch.getName() : patchedCertificateDto.getName();
        String description = patchedCertificateDto.getDescription() == null ? certificateToPatch.getDescription() : patchedCertificateDto.getDescription();
        Integer duration = patchedCertificateDto.getDuration() == null ? certificateToPatch.getDuration() : patchedCertificateDto.getDuration();
        BigDecimal price = patchedCertificateDto.getPrice() == null ? certificateToPatch.getPrice() : patchedCertificateDto.getPrice();
        certificateToPatch.setName(certificateName);
        certificateToPatch.setDescription(description);
        certificateToPatch.setDuration(duration);
        certificateToPatch.setPrice(price);
        repository.update(certificateToPatch);
    }

    private Set<TagDto> saveOrUpdateCertificateTags(CertificateDto entityDto) {
        Set<TagDto> tags = entityDto.getTags();
        Set<TagDto> addedTags = new HashSet<>(tags.size());
        tags.forEach(tagDto -> {
            addedTags.add(tagService.findOrSave(tagDto));
        });
        return addedTags;
    }
}