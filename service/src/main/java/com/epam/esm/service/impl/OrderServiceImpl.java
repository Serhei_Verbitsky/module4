package com.epam.esm.service.impl;

import com.epam.esm.converter.DtoEntityConverter;
import com.epam.esm.converter.impl.OrderSaveRequest;
import com.epam.esm.dto.OrderDto;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Order;
import com.epam.esm.entity.User;
import com.epam.esm.entity.User_;
import com.epam.esm.exception.CertificateServiceException;
import com.epam.esm.exception.OperationError;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.repository.OrderRepository;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.service.OrderService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.epam.esm.exception.OperationError.ACCESS_DENIED;
import static com.epam.esm.exception.OperationError.CERTIFICATE_NOT_FOUND;
import static com.epam.esm.exception.OperationError.ORDER_NOT_FOUND;
import static com.epam.esm.exception.OperationError.USER_NOT_FOUND;

@Service
public class OrderServiceImpl extends AbstractService<OrderDto, Order> implements OrderService {
    private final UserRepository userRepository;
    private final GiftCertificateRepository certificateRepository;

    public OrderServiceImpl(UserRepository userRepository,
                            GiftCertificateRepository certificateRepository,
                            OrderRepository repository,
                            DtoEntityConverter<OrderDto, Order> converter) {
        super(repository, converter);
        this.userRepository = userRepository;
        this.certificateRepository = certificateRepository;

    }

    @Override
    protected OperationError getNotFoundErrorCode() {
        return ORDER_NOT_FOUND;
    }

    @Transactional
    @Override
    public OrderDto save(OrderSaveRequest orderSaveRequest, Authentication authentication) {
        Order orderToSave = new Order();
        String userName = String.valueOf(authentication.getPrincipal());
        Optional<User> dbUser = userRepository.findByFieldName(User_.USER_NAME, userName);
        if (dbUser.isPresent()) {
            orderToSave.setUser(dbUser.get());
        } else {
            throw new CertificateServiceException(USER_NOT_FOUND, String.valueOf(orderSaveRequest.getUserId()));
        }
        if (!orderSaveRequest.getUserId().equals(dbUser.get().getId())) {
            throw new CertificateServiceException(ACCESS_DENIED);
        }
        List<GiftCertificate> certificateList = orderSaveRequest.getCertificates().stream()
                .map((cert_id) -> {
                    Optional<GiftCertificate> certificate = certificateRepository.findById(cert_id);
                    if (certificate.isPresent()) {
                        return certificate.get();
                    } else {
                        throw new CertificateServiceException(CERTIFICATE_NOT_FOUND, String.valueOf(cert_id));
                    }
                })
                .collect(Collectors.toList());
        orderToSave.setCertificates(certificateList);

        BigDecimal totalPrice = certificateList.stream()
                .map(GiftCertificate::getPrice)
                .reduce(new BigDecimal(0), BigDecimal::add);
        orderToSave.setOrderPrice(totalPrice);

        return converter.toDto(repository.save(orderToSave));
    }

    @Transactional(readOnly = true)
    @Override
    public OrderDto findById(Long id, Authentication authentication) {
        String userName = String.valueOf(authentication.getPrincipal());
        OrderDto requestedOrder = super.findById(id);
        if (isUserHasAdminRole(authentication) || userName.equals(requestedOrder.getUser().getUserName())) {
            return requestedOrder;
        }
        throw new CertificateServiceException(ACCESS_DENIED);
    }

    @Override
    public OrderDto save(OrderDto entityDto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(OrderDto entityDto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long id) {
        throw new UnsupportedOperationException();
    }
}