package com.epam.esm.service.impl;

import com.epam.esm.converter.DtoEntityConverter;
import com.epam.esm.dto.UserRole;
import com.epam.esm.exception.CertificateServiceException;
import com.epam.esm.exception.OperationError;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.CrudRepository;
import com.epam.esm.service.BaseService;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public abstract class AbstractService<D, E> implements BaseService<D> {
    protected CrudRepository<E> repository;
    protected DtoEntityConverter<D, E> converter;
    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    public AbstractService(CrudRepository<E> repository, DtoEntityConverter<D, E> converter) {
        this.repository = repository;
        this.converter = converter;
    }

    protected abstract OperationError getNotFoundErrorCode();

    @Transactional
    @Override
    public D save(D entityDto) {
        return converter.toDto(repository.save(converter.toEntity(entityDto)));
    }

    @Transactional
    @Override
    public void delete(Long id) {
        E toDelete = repository.findById(id)
                .orElseThrow(() -> new CertificateServiceException(getNotFoundErrorCode(), String.valueOf(id)));
        repository.delete(toDelete);
    }

    @Transactional(readOnly = true)
    @Override
    public D findById(Long id) {
        return repository.findById(id).map(converter::toDto)
                .orElseThrow(() -> new CertificateServiceException(getNotFoundErrorCode(), String.valueOf(id)));
    }

    @Transactional
    @Override
    public void update(D entityDto) {
        repository.update(converter.toEntity(entityDto));
    }

    @Transactional(readOnly = true)
    @Override
    public List<D> findAll(Map<String, String> parameterMap, PageParameter parameter) {
        parameter.setTotalPageCount(repository.calculateEntityCount(parameterMap, parameter));
        return converter.toDtoList(repository.findAll(parameterMap, parameter));
    }

    public boolean isUserHasAdminRole(Authentication authentication) {
        return authentication.getAuthorities().stream()
                .anyMatch(auth-> auth.getAuthority().equals(UserRole.ADMIN.getName()));
    }
}