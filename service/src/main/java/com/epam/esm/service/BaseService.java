package com.epam.esm.service;

import com.epam.esm.pageparameter.PageParameter;

import java.util.List;
import java.util.Map;

public interface BaseService<AbstractDto> {
    AbstractDto save(AbstractDto entityDto);

    void delete(Long id);

    AbstractDto findById(Long id);

    void update(AbstractDto entityDto);

    List<AbstractDto> findAll(Map<String, String> parameterMap, PageParameter parameter);
}