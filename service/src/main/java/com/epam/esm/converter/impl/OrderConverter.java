package com.epam.esm.converter.impl;

import com.epam.esm.dto.OrderDto;
import com.epam.esm.entity.Order;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderConverter extends AbstractConverter <OrderDto, Order> {
    public OrderConverter(ModelMapper modelMapper) {
        super(modelMapper);
    }

    @Override
    public Order toEntity(OrderDto dto) {
        return modelMapper.map(dto, Order.class);
    }

    @Override
    public OrderDto toDto(Order entity) {
        OrderDto result = modelMapper.map(entity, OrderDto.class);
        Optional.ofNullable(result.getUser()).ifPresent(user -> user.setOrders(null));
        return result;
    }
}