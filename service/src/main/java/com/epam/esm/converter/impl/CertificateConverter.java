package com.epam.esm.converter.impl;

import com.epam.esm.dto.CertificateDto;
import com.epam.esm.entity.GiftCertificate;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CertificateConverter extends AbstractConverter<CertificateDto, GiftCertificate> {

    public CertificateConverter(ModelMapper modelMapper) {
        super(modelMapper);
    }

    @Override
    public GiftCertificate toEntity(CertificateDto dto) {
        return modelMapper.map(dto, GiftCertificate.class);
    }

    @Override
    public CertificateDto toDto(GiftCertificate entity) {
        return modelMapper.map(entity, CertificateDto.class);
    }
}