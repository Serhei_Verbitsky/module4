package com.epam.esm.converter.impl;

import com.epam.esm.converter.DtoEntityConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractConverter<AbstractDto, AbstractEntity> implements DtoEntityConverter <AbstractDto, AbstractEntity> {
    @Autowired
    final ModelMapper modelMapper;

    public AbstractConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public List<AbstractEntity> toEntityList(Collection<AbstractDto> dtoList) {
        return dtoList.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<AbstractDto> toDtoList(Collection<AbstractEntity> entityList) {
        return entityList.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public Set<AbstractEntity> toEntitySet(Collection<AbstractDto> dtoList) {
        return new HashSet<>(toEntityList(dtoList));
    }

    @Override
    public Set<AbstractDto> toDtoSet(Collection<AbstractEntity> entityList) {
        return new HashSet<>(toDtoList(entityList));
    }
}