package com.epam.esm.converter;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface DtoEntityConverter<E, D> {
    D toEntity(E dto);

    E toDto(D entity);

    List<D> toEntityList(Collection<E> dtoList);

    List<E> toDtoList(Collection<D> entityList);

    Set<D> toEntitySet(Collection<E> dtoList);

    Set<E> toDtoSet(Collection<D> entityList);
}