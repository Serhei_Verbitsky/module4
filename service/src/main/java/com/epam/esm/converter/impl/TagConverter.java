package com.epam.esm.converter.impl;

import com.epam.esm.dto.TagDto;
import com.epam.esm.entity.Tag;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TagConverter extends AbstractConverter<TagDto, Tag> {

    public TagConverter(ModelMapper modelMapper) {
        super(modelMapper);
    }

    @Override
    public Tag toEntity(TagDto dto) {
        return modelMapper.map(dto, Tag.class);
    }

    @Override
    public TagDto toDto(Tag entity) {
        return modelMapper.map(entity, TagDto.class);
    }
}