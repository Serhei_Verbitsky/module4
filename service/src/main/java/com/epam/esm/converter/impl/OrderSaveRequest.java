package com.epam.esm.converter.impl;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Set;

@Data
@Validated
public class OrderSaveRequest {
    @Min(value = 1, message = "40003")
    private Long userId;
    private Set<@Valid @Min(value = 1, message = "40003") Long> certificates;
}
