package com.epam.esm.converter.impl;

import com.epam.esm.dto.UserDto;
import com.epam.esm.entity.Order;
import com.epam.esm.entity.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserConverter extends AbstractConverter<UserDto, User> {

    public UserConverter(ModelMapper modelMapper) {
        super(modelMapper);
    }

    @Override
    public User toEntity(UserDto dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    public UserDto toDto(User entity) {
        return modelMapper.map(removeUserFromOrder(entity), UserDto.class);
    }

    private User removeUserFromOrder(User user) {
        for (Order order : user.getOrders()) {
            order.setUser(null);
        }
        return user;
    }
}