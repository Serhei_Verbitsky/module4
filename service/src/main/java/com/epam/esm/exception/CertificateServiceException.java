package com.epam.esm.exception;

@SuppressWarnings("serial")
public class CertificateServiceException extends AbstractException {

    public CertificateServiceException(OperationError operationError, String ... message) {
        super(operationError, message);
    }
}