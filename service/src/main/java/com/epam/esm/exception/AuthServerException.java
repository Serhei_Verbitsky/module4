package com.epam.esm.exception;

@SuppressWarnings("serial")
public class AuthServerException extends AbstractException{
    public AuthServerException(OperationError operationError, String... errorDetails) {
        super(operationError, errorDetails);
    }
}