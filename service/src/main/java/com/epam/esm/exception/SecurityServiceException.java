package com.epam.esm.exception;

@SuppressWarnings("serial")
public class SecurityServiceException extends AbstractException {
    public SecurityServiceException(OperationError operationError, String... message) {
        super(operationError, message);
    }

    public SecurityServiceException(Throwable cause, OperationError operationError, String... message) {
        super(operationError, message, cause);
    }
}