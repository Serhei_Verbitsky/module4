package com.epam.esm.security;

import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.esm.exception.OperationError.ACCESS_DENIED;

@Component
public class BasicAccessDeniedHandler implements AccessDeniedHandler {
    private static final String DEFAULT_BAD_REQUEST_CODE = "40000";

    private final MessageSource messageSource;

    @Autowired
    public BasicAccessDeniedHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        JSONObject responseBody = new JSONObject();
        String errorMessage;
        try {
            errorMessage = messageSource.getMessage(ACCESS_DENIED.getErrorCode(), new Object[]{}, request.getLocale());

        } catch (Exception exception) {
            errorMessage = messageSource.getMessage(DEFAULT_BAD_REQUEST_CODE, new Object[]{}, request.getLocale());
        }
        String query = request.getRequestURL().toString() + "?" +
                (request.getQueryString() == null ? "" : request.getQueryString());
        responseBody.put("message", errorMessage);
        responseBody.put("code", ACCESS_DENIED.getErrorCode());
        responseBody.put("query", query);
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(responseBody.toString());
    }
}