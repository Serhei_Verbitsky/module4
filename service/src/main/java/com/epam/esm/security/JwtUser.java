package com.epam.esm.security;

import com.epam.esm.dto.UserDto;
import org.springframework.security.core.userdetails.User;

@SuppressWarnings("serial")
public class JwtUser extends User {
    public JwtUser(UserDto userDto) {
        super(userDto.getName(), userDto.getPassword(), userDto.getAuthorities());
    }
}
