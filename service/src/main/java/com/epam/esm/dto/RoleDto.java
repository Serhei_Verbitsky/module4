package com.epam.esm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDto extends AbstractDto {
    private static final long serialVersionUID = 811392690222013525L;

    private String name;
}
