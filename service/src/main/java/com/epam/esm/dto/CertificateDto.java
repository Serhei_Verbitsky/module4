package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CertificateDto extends AbstractDto {
    private static final long serialVersionUID = 6186629537657939891L;

    @NotNull(message = "40005")
    @Pattern(regexp = "[\\u0410-\\u044f\\w\\d\\s?!._'\\-]{3,70}", message = "40005")
    private String name;

    @Pattern(regexp = "[\\u0410-\\u044f\\w\\d\\s?!._'\\-]{3,270}", message = "40007")
    @NotNull(message = "40007")
    private String description;

    @DecimalMin(value = "0.00", inclusive = false, message = "40008")
    @NotNull(message = "40008")
    private BigDecimal price;

    @Min(value = 1, message = "40009")
    @NotNull(message = "40009")
    private Integer duration;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS", timezone = "UTC")
    private LocalDateTime createDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS", timezone = "UTC")
    private LocalDateTime lastUpdateDate;
    private Set<@Valid TagDto> tags;
}