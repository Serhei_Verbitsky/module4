package com.epam.esm.dto;

import java.util.Arrays;
import java.util.Optional;

public enum UserRole {
    DEFAULT_ROLE("ROLE_USER"),
    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER");

    private final String name;

    UserRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static UserRole resolve(String roleName) {
        Optional<UserRole> role = Arrays.stream(values())
                .filter(userRole -> userRole != DEFAULT_ROLE)
                .filter(userRole -> userRole.getName().equals(roleName))
                .findFirst();
        return role.orElseThrow(IllegalArgumentException::new);
    }
}