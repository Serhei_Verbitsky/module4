package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto extends AbstractDto implements OAuth2User {
    private static final long serialVersionUID = -8267225481311897527L;

    @NotNull(message = "40006")
    @Pattern(regexp = "[\\w\\d\\s_\\-]{3,70}", message = "40006")
    private String userName;

    @NotNull(message = "40011")
    @Pattern(regexp = "[\\w\\d?!._'\\-]{6,70}", message = "40011")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private UserRole role;

    private List<OrderDto> orders;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public Map<String, Object> getAttributes() {
        HashMap<String, Object> attr = new HashMap<>();
        attr.put("name", userName);
        attr.put("role", role);
        return attr;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<>(Collections.singletonList(new SimpleGrantedAuthority(role.getName())));
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Override
    public String getName() {
        return userName;
    }
}