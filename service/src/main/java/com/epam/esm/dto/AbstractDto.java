package com.epam.esm.dto;

import com.epam.esm.validator.OnCreate;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.Min;
import javax.validation.constraints.Null;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractDto extends RepresentationModel <AbstractDto> implements Serializable {
    private static final long serialVersionUID = 82495845406625646L;

    @Getter
    @Setter
    @Null(groups = OnCreate.class, message = "40010")
    @Min(value = 1, message = "40003")
    private Long id;
}