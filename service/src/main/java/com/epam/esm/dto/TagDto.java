package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TagDto extends AbstractDto {
    private static final long serialVersionUID = 4632394777723380775L;

    @NotNull(message = "40004")
    @Pattern(regexp = "[\\u0410-\\u044f\\w\\d\\s?!._'\\-]{3,70}", message = "40004")
    private String name;
}