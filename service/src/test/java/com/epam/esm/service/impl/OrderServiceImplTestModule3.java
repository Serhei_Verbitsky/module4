package com.epam.esm.service.impl;

import com.epam.esm.converter.impl.OrderConverter;
import com.epam.esm.converter.impl.OrderSaveRequest;
import com.epam.esm.dto.OrderDto;
import com.epam.esm.dto.UserDto;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Order;
import com.epam.esm.entity.User;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.repository.OrderRepository;
import com.epam.esm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class OrderServiceImplTestModule3 {

    @Mock
    private OrderConverter converter;

    @Mock
    private OrderRepository repository;

    @Mock
    private GiftCertificateRepository giftCertificateRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private OrderServiceImpl service;

    private Order order1;

    private OrderDto orderDto1;

    private List<OrderDto> dtoList;
    private List<Order> orderList;

    private GiftCertificate certificate;

    private OrderSaveRequest orderSaveRequest;

    private User user;
    private UserDto userDto;

    private PageParameter pageParameter;
    private Map<String, String> parameters;

    @BeforeEach
    public void init() {
        pageParameter = new PageParameter(1, 100, new ArrayList<>());
        parameters = new HashMap<>();

        certificate = new GiftCertificate();
        certificate.setId(1L);
        certificate.setPrice(new BigDecimal(10));

        userDto = new UserDto();
        userDto.setId(1L);
        userDto.setUserName("user1");

        user = new User();
        user.setId(1L);
        user.setUserName("user1");

        orderSaveRequest = new OrderSaveRequest();
        orderSaveRequest.setCertificates(new HashSet<>(Collections.singletonList(1L)));
        orderSaveRequest.setUserId(1L);

        order1 = new Order();
        order1.setId(1L);
        order1.setUser(user);
        order1.setOrderPrice(new BigDecimal(10));

        orderDto1 = new OrderDto();
        orderDto1.setId(1L);
        orderDto1.setUser(userDto);
        orderDto1.setPrice(new BigDecimal(10));

        orderList = new ArrayList<>(Collections.singletonList(order1));
        dtoList = new ArrayList<>(Collections.singletonList(orderDto1));

        when(repository.findById(any())).thenReturn(Optional.ofNullable(order1));
        when(converter.toDto(any())).thenReturn(orderDto1);
        when(converter.toEntity(any())).thenReturn(order1);
        when(converter.toDtoList(any())).thenReturn(dtoList);
    }

    @Test
    public void testSavePositiveResult() {
        when(userRepository.findByFieldName(any(), any())).thenReturn(Optional.ofNullable(user));
        when(giftCertificateRepository.findById(any())).thenReturn(Optional.ofNullable(certificate));
        when(repository.save(any())).thenReturn(order1);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken("user", null);
        OrderDto actual = service.save(orderSaveRequest, authentication);
        assertEquals(orderDto1, actual);
    }

    @Test
    public void testFindAllPositiveResult() {
        when(repository.findAll(any(), any())).thenReturn(orderList);
        List<OrderDto> actual = service.findAll(parameters, pageParameter);
        assertEquals(actual, dtoList);
    }

    @Test
    public void testFindByIdPositiveResult() {
        OrderDto actual = service.findById(1L);
        assertEquals(orderDto1, actual);
    }

    @Test
    public void testDeleteShouldTrowExceptionWithStatusCodeNotSupported() {
        assertThrows(UnsupportedOperationException.class, () -> service.delete(1L));
    }

    @Test
    public void testUpdateTagsShouldTrowExceptionWithStatusCodeNotSupported() {
        assertThrows(UnsupportedOperationException.class, () -> service.update(new OrderDto()));
    }
}