package com.epam.esm.service.impl;

import com.epam.esm.converter.impl.UserConverter;
import com.epam.esm.dto.OrderDto;
import com.epam.esm.dto.UserDto;
import com.epam.esm.entity.Order;
import com.epam.esm.entity.Role;
import com.epam.esm.entity.User;
import com.epam.esm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserServiceImplTestSave {
    @Mock
    private UserConverter converter;

    @Mock
    private UserRepository repository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl service;

    private User user;
    private UserDto userDto;

    private Role role;


    @BeforeEach
    public void init() {
        role = new Role();
        role.setName("ROLE_USER");
        role.setId(2L);

        Order order = new Order();
        order.setUser(new User());
        OrderDto orderDto = new OrderDto();
        orderDto.setUser(new UserDto());

        user = new User();
        user.setId(1L);
        user.setUserName("user");
        user.setOrders(new ArrayList<>(Collections.singletonList(order)));

        userDto = new UserDto();
        userDto.setId(1L);
        userDto.setUserName("user");
        userDto.setPassword("password");
        userDto.setOrders(new ArrayList<>(Collections.singletonList(orderDto)));
    }

    @Test
    public void testSavePositiveResult() {
        when(repository.findByFieldName(any(), any())).thenReturn(Optional.empty());
        when(converter.toEntity(any(UserDto.class))).thenReturn(user);
        when(passwordEncoder.encode(any())).thenReturn("password");
        when(repository.save(any(User.class))).thenReturn(user);
        when(repository.findById(any())).thenReturn(Optional.ofNullable(user));
        when(converter.toDto(any(User.class))).thenReturn(userDto);

        UserDto actual = service.save(userDto);
        assertEquals(actual, userDto);
        verify(repository).save(any());
    }
}