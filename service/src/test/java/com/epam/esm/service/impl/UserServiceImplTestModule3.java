package com.epam.esm.service.impl;

import com.epam.esm.converter.impl.UserConverter;
import com.epam.esm.dto.OrderDto;
import com.epam.esm.dto.UserDto;
import com.epam.esm.entity.Order;
import com.epam.esm.entity.User;
import com.epam.esm.exception.CertificateServiceException;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.epam.esm.exception.OperationError.USER_ALREADY_EXISTS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserServiceImplTestModule3 {

    @Mock
    private UserConverter converter;

    @Mock
    private UserRepository repository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl service;

    private User user;
    private UserDto userDto;

    private PageParameter pageParameter;
    private Map<String, String> parameters;
    private List<UserDto> userDtoList;

    @BeforeEach
    public void init() {
        pageParameter = new PageParameter(1, 100, new ArrayList<>());
        parameters = new HashMap<>();

        Order order = new Order();
        order.setUser(new User());
        OrderDto orderDto = new OrderDto();
        orderDto.setUser(new UserDto());

        user = new User();
        user.setId(1L);
        user.setUserName("user");

        user.setOrders(new ArrayList<>(Collections.singletonList(order)));

        userDto = new UserDto();
        userDto.setId(1L);
        userDto.setUserName("user");
        userDto.setOrders(new ArrayList<>(Collections.singletonList(orderDto)));
        userDtoList = new ArrayList<>(Collections.singletonList(userDto));

        when(repository.findById(any(Long.class))).thenReturn(Optional.ofNullable(user));
        when(passwordEncoder.encode(any())).thenReturn("password");
    }

    @Test
    public void testSavePositiveResultReturnsExistingValue() {
        when(repository.findByFieldName(any(), any())).thenReturn(Optional.ofNullable(user));
        UserDto addedDto = new UserDto();
        addedDto.setId(1L);
        addedDto.setUserName("user");
        addedDto.setOrders(new ArrayList<>(Collections.singletonList(new OrderDto())));
        CertificateServiceException exception = assertThrows(CertificateServiceException.class,
                () -> service.save(addedDto));
        assertEquals(exception.getOperationError(), USER_ALREADY_EXISTS);
    }

    @Test
    public void testFindByIdPositiveResult() {
        when(converter.toDto(any())).thenReturn(userDto);
        UserDto actual = service.findById(1L);
        assertEquals(actual.getUserName(), userDto.getUserName());
    }

    @Test
    public void testFindAllPositiveResult() {
        when(converter.toDtoList(any())).thenReturn(userDtoList);
        List<UserDto> actual = service.findAll(parameters, pageParameter);
        assertEquals(actual, userDtoList);
    }

    @Test
    public void testDeleteShouldTrowExceptionWithStatusCodeNotSupported() {
        assertThrows(UnsupportedOperationException.class, () -> service.delete(1L));
    }

    @Test
    public void testUpdateTagsShouldTrowExceptionWithStatusCodeNotSupported() {
        assertThrows(UnsupportedOperationException.class, () -> service.update(new UserDto()));
    }
}