package com.epam.esm.service.impl;

import com.epam.esm.converter.impl.CertificateConverter;
import com.epam.esm.dto.CertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.exception.CertificateServiceException;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.impl.GiftCertificateRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.epam.esm.exception.OperationError.CERTIFICATE_NOT_FOUND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class GiftCertificateServiceImplTestModule3 {

    @Mock
    private GiftCertificateRepositoryImpl repository;

    @Mock
    private CertificateConverter certificateConverter;

    @Mock
    private TagServiceImpl tagService;

    @InjectMocks
    private GiftCertificateServiceImpl service;

    private GiftCertificate certificate;
    private CertificateDto certificateDto;
    private Set<TagDto> dtoTagsSet;
    private Set<Tag> tags;
    private TagDto tagDto;
    private PageParameter pageParameter;
    private Map<String, String> parameters;

    @BeforeEach
    public void setUp() {
        certificateDto = new CertificateDto();
        certificateDto.setName("name1");
        certificateDto.setId(1L);
        certificateDto.setDescription("description1");
        certificateDto.setDuration(1);
        certificateDto.setPrice(new BigDecimal(1));

        certificate = new GiftCertificate();
        certificate.setId(1L);
        certificate.setName("name1");
        certificate.setDescription("description1");
        certificate.setDuration(1);
        certificate.setPrice(new BigDecimal(1));

        pageParameter = new PageParameter(1, 100, new ArrayList<>());
        parameters = new HashMap<>();

        dtoTagsSet = new HashSet<>();
        tagDto = new TagDto();
        tagDto.setId(1L);
        tagDto.setName("tag1");
        dtoTagsSet.add(tagDto);
        tags = new HashSet<>();
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("tag1");
        tags.add(tag1);

        certificate.setTags(tags);
        certificateDto.setTags(new HashSet<>(dtoTagsSet));

        when(certificateConverter.toDto(any(GiftCertificate.class))).thenReturn(certificateDto);
        when(certificateConverter.toDtoList(any()))
                .thenReturn(new ArrayList<>(Collections.singletonList(certificateDto)));
        when(repository.findById(any())).thenReturn(Optional.ofNullable(certificate));
    }

    @Test
    public void testSavePositiveResult() {
        certificate.setTags(tags);
        when(tagService.findOrSave(any())).thenReturn(tagDto);
        when(repository.save(any())).thenReturn(certificate);
        when(certificateConverter.toDto(any(GiftCertificate.class))).thenReturn(certificateDto);
        CertificateDto actual = service.save(certificateDto);
        assertEquals(actual.getId(), certificateDto.getId());
    }

    @Test
    public void testDeletePositiveResult() {
        service.delete(1L);
        verify(repository, times(1)).delete(any());
    }

    @Test
    public void testDeleteNegativeShouldThrowCertificateServiceException() {
        when(repository.findById(1L)).thenReturn(Optional.empty());
        CertificateServiceException exception = assertThrows(CertificateServiceException.class,
                () -> service.delete(1L));
        assertEquals(exception.getOperationError(), CERTIFICATE_NOT_FOUND);
    }

    @Test
    public void testFindAllPositiveResult() {
        when(repository.findAll(any(), any())).thenReturn(new ArrayList<>(Collections.singletonList(certificate)));
        when(certificateConverter.toDto(any(GiftCertificate.class))).thenReturn(certificateDto);
        List<CertificateDto> expected = new ArrayList<>(Collections.singletonList(certificateDto));
        List<CertificateDto> actual = service.findAll(parameters, pageParameter);
        assertEquals(expected.get(0).getId(), (actual.get(0).getId()));
    }

    @Test
    public void testFindByIdPositiveResult() {
        when(repository.findById(any())).thenReturn(Optional.ofNullable(certificate));
        CertificateDto expected = certificateDto;
        CertificateDto actual = service.findById(1L);
        assertEquals(expected.getId(), actual.getId());
    }

    @Test
    public void testFindByIdNegativeShouldThrowCertificateServiceException() {
        when(repository.findById(1L)).thenReturn(Optional.empty());
        CertificateServiceException exception = assertThrows(CertificateServiceException.class,
                () -> service.findById(1L));
        assertEquals(exception.getOperationError(), CERTIFICATE_NOT_FOUND);
    }

    @Test
    public void testUpdatePositiveResult() {
        doNothing().when(repository).update(any());
        when(certificateConverter.toEntity(any(CertificateDto.class))).thenReturn(certificate);
        service.update(certificateDto);
        verify(repository, times(1)).update(any());
    }

    @Test
    void testPatchPositive() {
        service.patch(certificateDto);
        verify(repository).update(any());
    }
}