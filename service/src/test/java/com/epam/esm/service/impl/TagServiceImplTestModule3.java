package com.epam.esm.service.impl;

import com.epam.esm.converter.impl.TagConverter;
import com.epam.esm.dto.TagDto;
import com.epam.esm.entity.Tag;
import com.epam.esm.exception.CertificateServiceException;
import com.epam.esm.pageparameter.PageParameter;
import com.epam.esm.repository.TagRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.epam.esm.exception.OperationError.TAG_NOT_FOUND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class TagServiceImplTestModule3 {

    @Mock
    private TagConverter converter;

    @Mock
    private TagRepository repository;

    @InjectMocks
    private TagServiceImpl service;

    private Tag tag1;
    private Tag tag2;

    private TagDto tagDto1;
    private TagDto tagDto2;

    private List<TagDto> dtoList;
    private List<Tag> tagList;

    private PageParameter pageParameter;
    private Map<String, String> parameters;

    @BeforeEach
    public void init() {
        pageParameter = new PageParameter(1, 100, new ArrayList<>());
        parameters = new HashMap<>();
        tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("tag1");
        tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("tag2");

        tagDto1 = new TagDto();
        tagDto1.setId(1L);
        tagDto1.setName("tag1");
        tagDto2 = new TagDto();
        tagDto2.setId(2L);
        tagDto2.setName("tag2");

        tagList = new ArrayList<>(Arrays.asList(tag1, tag2));
        dtoList = new ArrayList<>(Arrays.asList(tagDto1, tagDto2));

        when(repository.findByFieldName("name", tag1.getName())).thenReturn(Optional.ofNullable(tag1));
        when(repository.findByFieldName("name", tag2.getName())).thenReturn(Optional.ofNullable(tag2));
        when(repository.findById(1L)).thenReturn(Optional.ofNullable(tag1));
        when(repository.findById(2L)).thenReturn(Optional.ofNullable(tag1));
        when(repository.save(tag1)).thenReturn(tag1);
        when(repository.save(tag2)).thenReturn(tag2);

        when(converter.toDtoList(any())).thenReturn(dtoList);
        when(converter.toDto(tag1)).thenReturn(tagDto1);
        when(converter.toDto(tag2)).thenReturn(tagDto2);
    }

    @Test
    public void testSavePositiveResult() {
        when(repository.save(any())).thenReturn(tag1);
        when(converter.toEntity(any())).thenReturn(tag1);
        TagDto addedDto = new TagDto();
        addedDto.setId(1L);
        addedDto.setName("tag1");
        TagDto actual = service.save(addedDto);
        assertEquals(actual.getName(), tag1.getName());
    }

    @Test
    public void testFindOrSavePositiveResultReturnsExistingValue() {
        when(converter.toEntity(any())).thenReturn(tag1);
        when(repository.findByFieldName(any(), any())).thenReturn(Optional.ofNullable(tag1));
        TagDto addedDto = new TagDto();
        addedDto.setId(1L);
        addedDto.setName("tag1");
        TagDto actual = service.findOrSave(addedDto);
        assertEquals(actual.getName(), tag1.getName());
    }

    @Test
    public void testFindOrSavePositiveResultShouldCallRepositorySaveMethod() {
        when(repository.save(any())).thenReturn(tag1);
        when(converter.toEntity(any())).thenReturn(tag1);
        when(repository.findByFieldName(any(), any())).thenReturn(Optional.empty());
        TagDto addedDto = new TagDto();
        addedDto.setId(1L);
        addedDto.setName("tag1");
        TagDto actual = service.findOrSave(addedDto);
        assertEquals(actual.getName(), tag1.getName());
        verify(repository).save(any());
    }

    @Test
    public void testFindAllPositiveResult() {
        when(repository.findAll(any(), any())).thenReturn(tagList);
        List<TagDto> actual = service.findAll(parameters, pageParameter);
        assertEquals(actual, dtoList);
    }

    @Test
    public void testFindByIdPositiveResult() {
        TagDto actual = service.findById(1L);
        assertEquals("tag1", actual.getName());
    }

    @Test
    public void testDeletePositiveResult() {
        service.delete(1L);
        verify(repository, times(1)).delete(any());
    }

    @Test
    public void testDeleteNegativeShouldThrowCertificateServiceException() {
        when(repository.findById(1L)).thenReturn(Optional.empty());
        CertificateServiceException exception = assertThrows(CertificateServiceException.class,
                () -> service.delete(1L));
        assertEquals(exception.getOperationError(), TAG_NOT_FOUND);
    }

    @Test
    public void testUpdateTagsShouldTrowExceptionWithStatusCodeNotSupported() {
         assertThrows(UnsupportedOperationException.class, () -> service.update(new TagDto()));
    }


    @Test
    public void testFindUserMostUsedTag() {
        when(repository.findUserMostUsedTag()).thenReturn(tag1);
        TagDto actual = service.findUserMostUsedTag();
        assertEquals(tagDto1, actual);
    }
}